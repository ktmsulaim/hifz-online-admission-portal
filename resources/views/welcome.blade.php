<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Darul Hasanath Hifz al Quran College | Home</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('css/home.css')}}">
    <link rel="shortcut icon" href="{{asset('img/logo.png')}}" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,900;1,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;400;600;900&display=swap" rel="stylesheet">
</head>
<body>

    <header id="main">
        <nav>
            <div class="brand">
                DHHQC
            </div>
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a id="home" class="button" href="{{ url('/home') }}">Home</a>
                    @else
                        <a id="login" class="button" href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a id="register" class="button" href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif
        </nav>

        <div class="content">
            <div class="quote">
                <span class="college ar">كلية دار الحسانات الإسلامية لتحفيظ القرآن الكريم</span>
                <div class="ar qala">
                    قال رسول الله صلى الله عليه وسلم
                </div>
                <div class="main-qala">
                    <div class="main-qala-text ar"><small>(‏رواه البخاري‏)</small> ‏خيركم من تعلم القرآن وعلمه</div>
                </div>
                <div class="eng">
                    "The best amongst you is the one who learns the Qur'an and teaches it."
                </div>
                <span class="quote-pic">
                    <img src="{{asset('img/q.png')}}" width="50" height="50" alt="">
                </span>
            </div>

            @if ($current && $current->status === 1)
            <div class="apply-btn">
                <p>Admission to {{$current->name}} is open!</p>
                <a href="{{route('application.create')}}" class="button" id="apply">Apply now</a>
            </div>
        @endif
        </div>
    </header>
    <main>
        <section class="section lite" id="howto">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="section-head">
                            <h2>how to apply</h1>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 m-auto">
                        <div class="image">
                            <img src="{{asset('img/bg2.jpg')}}" class="img-fluid" alt="">
                        </div>
                        <div class="steps">
                            <ol>
                                <li>Create an account</li>
                                <li>Click on apply now, and fill the application form</li>
                                <li>Wait to get verified</li>
                                <li>Select a time to appear in the examination</li>
                                <li>Stay tuned for results.</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        @if ($current && $current->hasResult())
            @if ($current->results->first()->status === 1)
            <section class="section dark" id="result">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="section-head">
                                <h2 class="text-white">Results</h2>
                            </div>
                            <div class="details text-white text-center">
                                <p>The {{$current->results->first()->name}} has been published! <a href="/result">Check now</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            @endif
        @endif
    </main>
    <footer>
        <span>{{date('Y')}} &copy; Darul Hasanath Islamic Complex. All rights reserved. Developed by <a href="http://sacreationsknr.info">SA Creations</a></span>
    </footer>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</body>
</html>
