@extends('layouts.client')

@section('page_title')
    <h1>
        Edit {{$app->refno()}}
    </h1>
@endsection

@section('breadcrumb')
    <li><a href="{{route('application.index')}}">Applications</a></li>
    <li><a>Edit</a></li>
    <li class="active"><a href="{{route('application.show', $app->id)}}">{{$app->refno()}}</a></li>
@endsection

@section('style')
    <style>
        .kv-file-content{
            width: 100% !important;
        }
    </style>
@endsection


@section('content')
    <div class="row">
        <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
            @if (count($errors) > 0)
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Errors</h3>
                    </div>
                    <div class="box-body">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Upload image</h3>
                </div>
                <div class="box-body" id="profile">
                    <div>
                        <input type="file" form="app" name="photo" id="photo_id" accept=".jpg,.gif,.png, .jpeg">
                    </div>
                    <div class="info text-center">
                        (Passport size, max.size: 512kb)
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Personal info</h3>
                </div>
                <div class="box-body">
                    <form id="app" action="{{route('application.update', $app->id)}}" method="post" class="app-form" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')
                        <input type="hidden" name="session_id" value="{{$current->id}}">
                        <input type="hidden" name="ref_no" value="{{$app->ref_no}}">
                        <input type="hidden" name="user_id" value="{{Auth::id()}}">

                        <p><small>All fields are required!</small></p>
                        <div class="form-group">
                            <label for="name">Full name</label>
                            <input type="text" class="form-control" name="name" id="name" value="{{old('name', $app->name)}}" maxlength="40" placeholder="Full name of applicant" required>
                        </div>

                        {{-- <div class="form-group row">
                            <div class="col-md-2 col-sm-12 col-xs-12">
                                <label for="name_of_home">Name of house</label>
                            </div>
                           <div class="col-md-10 col-sm-12 col-xs-12">
                                <input type="text" class="form-control" name="name_of_home" id="name_of_home" value="{{old('name_of_home')}}" maxlength="40" placeholder="Name of house" required>
                           </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-2 col-sm-12 col-xs-12">
                                <label for="name_of_father">Name of father</label>
                            </div>
                           <div class="col-md-10 col-sm-12 col-xs-12">
                                <input type="text" class="form-control" name="name_of_father" id="name_of_father" value="{{old('name_of_father')}}" maxlength="40" placeholder="Name of father" required>
                           </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-2 col-sm-12 col-xs-12">
                                <label for="job_of_father">Job of father</label>
                            </div>
                           <div class="col-md-10 col-sm-12 col-xs-12">
                                <input type="text" class="form-control" name="job_of_father" id="job_of_father" value="{{old('job_of_father')}}" maxlength="40" placeholder="Job of father" required>
                           </div>
                        </div> --}}


                        <div class="form-group row">
                            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="dob">DOB</label>
                                    <input type="text" class="form-control" name="dob" id="dob" value="{{old('dob', $app->dob)}}" placeholder="Date of birth" required>
                               </div>
                            </div>
                            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="dob_hijri">Age on {{$current->to}}</label>
                                    <input type="text" name="dob_details" class="form-control" id="dob_details" value="{{old('dob_details', $app->dob_details)}}" readonly>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        {{-- <p><input type="checkbox" id="not_father"> <label for="not_father">Father is not parent</label></p> --}}
                       {{-- <div id="guard">
                        <hr>
                        <div class="form-group row">
                            <div class="col-md-2 col-sm-12 col-xs-12">
                                <label for="reason_not_father">Reason</label>
                            </div>
                           <div class="col-md-10 col-sm-12 col-xs-12">
                                <input type="text" class="form-control" name="reason_not_father" id="reason_not_father" value="{{old('reason_not_father')}}" maxlength="40" placeholder="The reason of parent for not being father">
                           </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-md-2 col-sm-12 col-xs-12">
                                <label for="name_of_guard">Name of guardian</label>
                            </div>
                           <div class="col-md-10 col-sm-12 col-xs-12">
                                <input type="text" class="form-control" name="name_of_guard" id="name_of_guard" value="{{old('name_of_guard')}}" maxlength="40" placeholder="Name of guardian">
                           </div>
                        </div>



                        <div class="form-group row">
                            <div class="col-md-2 col-sm-12 col-xs-12">
                                <label for="relation_of_guard">Relation of guardian</label>
                            </div>
                           <div class="col-md-10 col-sm-12 col-xs-12">
                                <input type="text" class="form-control" name="relation_of_guard" id="relation_of_guard" value="{{old('relation_of_guard')}}" maxlength="40" placeholder="Relation of guardian with applicant">
                           </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-md-2 col-sm-12 col-xs-12">
                                <label for="job_of_guard">Job of guardian</label>
                            </div>
                           <div class="col-md-10 col-sm-12 col-xs-12">
                                <input type="text" class="form-control" name="job_of_guard" id="job_of_guard" value="{{old('job_of_guard')}}" maxlength="40" placeholder="Job of guardian">
                           </div>
                        </div>
                        <hr>
                    </div> --}}

                        {{-- <div class="form-group row">
                            <div class="col-md-2 col-sm-12 col-xs-12">
                                <label for="name_of_mahall">Name of Mahall</label>
                            </div>
                           <div class="col-md-10 col-sm-12 col-xs-12">
                                <input type="text" class="form-control" name="name_of_mahall" id="name_of_mahall" value="{{old('name_of_mahall')}}" maxlength="40" placeholder="Name of Mahall" required>
                           </div>
                        </div> --}}


                        <div class="form-group row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="district">District</label>
                                    <input type="text" class="form-control" name="district" id="district" value="{{old('district', $app->district)}}" maxlength="40" placeholder="District" required>
                                </div>
                            </div>
                           <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="province">Province</label>
                                <input type="text" class="form-control" name="province" id="province" value="{{old('province', $app->province)}}" maxlength="40" placeholder="Province" required>
                            </div>
                           </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="village">Village</label>
                                    <input type="text" class="form-control" name="village" id="village" value="{{old('village', $app->village)}}" maxlength="40" placeholder="Village" required>
                                </div>
                            </div>
                           <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="mobile">Mobile</label>
                                <input type="number" min="0" class="form-control" name="mobile" id="mobile" value="{{old('mobile', $app->mobile)}}" maxlength="40" placeholder="Mobile number without country code" required>
                            </div>
                           </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="landphone_std">Landphone STD</label>
                                    <input type="number" min="0" class="form-control" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" name="landphone_std" id="landphone_std" value="{{old('landphone_std', $app->landphone_std)}}" maxlength="4" placeholder="Landphone STD" required>
                                </div>
                            </div>
                           <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="landphone">Landphone</label>
                                <input type="number" min="0" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" class="form-control" name="landphone" id="landphone" value="{{old('landphone', $app->landphone)}}" maxlength="7" placeholder="Landphone number if any" required>
                            </div>
                           </div>
                        </div>

                        <div class="form-group">
                            <label for="name_of_madrassa">Name of Madrassa</label>
                            <input type="text" class="form-control" name="name_of_madrassa" id="name_of_madrassa" value="{{old('name_of_madrassa', $app->name_of_madrassa)}}" maxlength="40" placeholder="Name of Madrassa" required>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="madrassa_completed_class">Completed class</label>
                                    <input type="number" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" min="0" class="form-control" name="madrassa_completed_class" id="madrassa_completed_class"  value="{{old('madrassa_completed_class', $app->madrassa_completed_class)}}" maxlength="2" placeholder="Completed class from Madrassa" required>
                                </div>
                            </div>
                           <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="madrassa_number">Madrassa number</label>
                                <input type="number" min="0" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" class="form-control" name="madrassa_number" id="madrassa_number" value="{{old('madrassa_number', $app->madrassa_number)}}" maxlength="10" placeholder="Approval number of Madrassa" required>
                            </div>
                           </div>
                        </div>


                        {{-- <div class="form-group row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="name_of_range">Name of range</label>
                                    <input type="text" class="form-control" name="name_of_range" id="name_of_range" value="{{old('name_of_range')}}" maxlength="40" placeholder="Name of range" required>
                                </div>
                            </div>
                           <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="date_of_exam">Date of exam</label>
                                <input type="text" class="form-control" name="date_of_exam" id="date_of_exam" value="{{old('date_of_exam')}}" maxlength="40" placeholder="Date of exam in 3rd class" required>
                            </div>
                           </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="name_of_school">Name of school</label>
                                    <input type="text" class="form-control" name="name_of_school" id="name_of_school" value="{{old('name_of_school')}}" maxlength="40" placeholder="Name of school" required>
                                </div>
                            </div>
                           <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="school_completed_class">Completed class</label>
                                <input type="number" min="0" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" class="form-control" name="school_completed_class" id="school_completed_class" value="{{old('school_completed_class')}}" maxlength="2" placeholder="Completed class from school" required>
                            </div>
                           </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="identification_mark1">Identification mark 1</label>
                                    <input type="text" class="form-control" name="identification_mark1" id="identification_mark1" value="{{old('identification_mark1')}}" maxlength="40" placeholder="Identification mark 1" required>
                                </div>
                            </div>
                           <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="form-group">
                                    <label for="identification_mark2">Identification mark 2</label>
                                    <input type="text" class="form-control" name="identification_mark2" id="identification_mark2" value="{{old('identification_mark2')}}" maxlength="40" placeholder="Identification mark 2" required>
                                </div>
                            </div>
                           </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-2 col-sm-12 col-xs-12">
                                <label for="postal_address">Postal address</label>
                            </div>
                           <div class="col-md-10 col-sm-12 col-xs-12">
                                <textarea name="postal_address" id="postal_address" style="resize:none" cols="30" rows="4" class="form-control" maxlength="180" required>{{old('postal_address')}}</textarea>
                           </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="post_office">Post office</label>
                                    <input type="text" class="form-control" name="post_office" id="post_office" value="{{old('post_office')}}" maxlength="30" placeholder="Post office" required>
                                </div>
                            </div>
                           <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="form-group">
                                    <label for="pincode">Pincode</label>
                                    <input type="number" min="0" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" class="form-control" name="pincode" id="pincode" value="{{old('pincode')}}" maxlength="6" placeholder="Pincode" required>
                                </div>
                            </div>
                           </div>
                        </div> --}}
                    </form>
                </div>
                <div class="box-footer text-right">
                    <button form="app" type="submit" class="btn btn-primary"><i class="fa fa-send fa-fw"></i> Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script>
        $(function(){
            $('#photo_id').fileinput({
                showUpload: false,
                maxFileSize: 512,
                allowedFileTypes: ['image'],
                allowedFileExtensions: ['jpg', 'jpeg', 'png', 'gif'],
                @if($app->hasPhoto())
                    initialPreview: '<img src="{{$app->photo->filename}}" width="100%" height="auto">'
                @endif
            });

            $('#dob').datepicker({
                format: 'dd/mm/yyyy',
                orientation: 'bottom',
                startDate: '{{$current->birth_min}}',
                endDate: '{{$current->birth_max}}',
                clearBtn: true
            });

            $('#dob').change(function(){
                var val = $(this).val();
                var dob = $('#dob_details');
                var target = '{{$current->to}}';
                if(val !== '') {
                    $.ajax({
                        url: '/calcage',
                        method: 'POST',
                        data: {
                            '_token': '{{csrf_token()}}',
                            'age':val,
                            'target': target
                        }
                    }).done(function(data){
                        dob.val(data);
                    });
                }
            });
        });
    </script>
@endsection
