<?php

namespace App\Http\Controllers;

use App\Applicant;
use App\Result;
use App\Session;
use App\Test;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class ApplicantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Applicant  $applicant
     * @return \Illuminate\Http\Response
     */
    public function show(Applicant $applicant)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Applicant  $applicant
     * @return \Illuminate\Http\Response
     */
    public function edit(Applicant $applicant)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Applicant  $applicant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Applicant $applicant)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Applicant  $applicant
     * @return \Illuminate\Http\Response
     */
    public function destroy(Applicant $applicant)
    {
        //
    }

    public function attendance()
    {
        if(Gate::allows('super_access')) {
            $current = Session::current();
            $applicants = $current->applicants;
            return view('admin.applications.applicants.attendance', compact('applicants'));
        } else {
            return back();
        }
    }

    public function mark(Request $request) {
        if(Gate::allows('super_access')){
            $current = Session::current();
            $apt = $current->applicants->find($request->applicant_id);
            if($apt) {
                $status = (int)$request->input('status');

                if($status === 1) {
                    $apt->section()->updateExistingPivot($request->input('section_id'), ['appear' => $status, 'appeartime' => Carbon::now()]);
                } else {
                    $apt->section()->updateExistingPivot($request->input('section_id'), ['appear' => $status]);
                }

                return back();

            }
        } else {
            return back();
        }
    }

    public function markentry()
    {
        if(Gate::allows('examination_access')) {
            $current = Session::current();
            $applicants = $current->applicants;
            return view('admin.examination.markentry', compact('current', 'applicants'));
        } else {
            return back();
        }
    }


    public function findapt(Request $request)
    {
        $applicant = Applicant::find($request->id);
        $current = Session::current();
        if($applicant && $applicant->eligibleForExam()){
            $max = $current->tests->sum('max_mark');
            return view('admin.examination.entrypage', compact('applicant', 'max'));
        } else {
            return back();
        }
    }

    public function updateMarks(Request $request) {
        $apt = Applicant::findOrFail($request->applicant);
        $data = $request->except(['_token', 'applicant', 'total', '_method']);

        foreach($data as $key => $val) {
            if(strpos($key, 'test') !== false){
                $test = Test::findOrFail(substr($key, 4, 1));
                $apt->tests()->updateExistingPivot($test->id, ['mark' => $val, 'status' => $val >= $test->pass_mark ? 1 : 0]);
            }

            if(strpos($key, 'rema') !== false){
                $apt->tests()->updateExistingPivot(substr($key, 4, 1), ['remark' => $val]);
            }
            // print_r(substr($key, 4, 1));
        }

        $apt->marks = $request->total;
        $apt->status = 2;
        $apt->update();
        Applicant::updateRank();
        return back()->with('success', 'Mark entry was successfull!');
    }

    public function wlist()
    {
        if(Gate::allows('applicants_access')){
            $current = Session::current();
            $applicants = $current->applicants()->where('applicants.status', '=', 2)->orderBy('marks', 'desc')->get();
            $max = $current->tests()->sum('max_mark');
            return view('admin.applications.applicants.waiting', compact('current', 'applicants', 'max'));
        } else {
            return back();
        }
    }

    public function addToEnrolled(Request $request)
    {
        if(Gate::allows('super_access')){
            $current = Session::current();
            if($request->ids !== null) {
                if($current->hasResult() === false) {
                    $result = $current->results()->create(['name' => 'Result of '.$current->name]);
                    $ids = $request->ids;
                    if(count($ids) <= $current->max_count){
                        foreach($ids as $id) {
                            $apt = Applicant::find($id);
                            $apt->result()->attach($result->id);
                            $apt->status = 1;
                            $apt->save();
                        }
                    } else {
                        return back()->with('exceeds', 'The maximum count of enrolling students are reached!');
                    }
                    return back();
                } else {
                    $result = $current->results->first();
                    $ids = $request->ids;
                    if(count($ids) <= $current->avail() && (count($result->applicants) - count($ids) < $current->avail())){
                        foreach($ids as $id) {
                            $apt = Applicant::find($id);
                            $apt->result()->attach($result->id);
                            $apt->status = 1;
                            $apt->save();
                        }
                        return back();
                    } else {
                        return back()->with('exceeds', 'The maximum count of enrolling students are reached!');
                    }
                }
            } else {
                return back();
            }
        } else {
            return back();
        }
    }

    public function enrolled()
    {
        if(Gate::allows('super_access')){
            $current = Session::current();
            if($current->hasResult()){
                $result = $current->results->first();
                $applicants = $result->applicants()->where('applicants.status', '=', 1)->orderBy('marks', 'desc')->get();
                $max = $current->tests->sum('max_mark');
                return view('admin.applications.applicants.enrolled', compact('current', 'result','applicants', 'max'));
            } else {
                return back();
            }
        } else {
            return back();
        }
    }

    public function removeFromList(Request $request)
    {
        if(Gate::allows('super_access')){
            $apt = Applicant::findOrFail($request->apt_id);
            $apt->result()->detach($request->result_id);
            $apt->status = 2;
            $apt->save();
            return back();
        } else {
            return back();
        }
    }
}
