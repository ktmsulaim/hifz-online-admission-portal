<?php

namespace App\Http\Controllers;

use App\Application;
use App\Http\Requests\ApplicationCreateRequest;
use App\Http\Requests\ApplicationUpdateRequest;
use App\Photo;
use App\Session;
use App\Test;
use App\Type;
use App\User;
use App\VerificationItem;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use setasign\Fpdi\Fpdi;

class AdminApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Gate::allows('application_access')) {
            $current = Session::current();
            $apps = $current->applications()->paginate(15);

            return view('admin.applications.index', compact('apps'));
        } else {
            return back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Gate::allows('application_access')) {
            $current = Session::current();
            $users = User::whereHas('role', function($role){
                $role->where('name', '=', 'standard');
            })->get();
            if($current) {
                return view('admin.applications.apply', compact('current', 'users'));
            } else {
                return back();
            }
        } else {
            return back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ApplicationCreateRequest $request)
    {
        if(Gate::allows('application_access')) {
            $session = Session::findOrFail($request->session_id);
            // Check birthday date
        if(!empty($request->dob)) {
            $dob = str_replace('/', '-', $request->dob);
            $dob = date('Y-m-d', strtotime($dob));

            $dob_min = date('Y-m-d', strtotime(str_replace('/', '-', $session->birth_min)));
            $dob_max = date('Y-m-d', strtotime(str_replace('/', '-', $session->birth_max)));

            if(Carbon::parse($dob)->lessThan(Carbon::parse($dob_min)) || Carbon::parse($dob)->greaterThan(Carbon::parse($dob_max))){
                return back();
            }
        }

        // upload photo
        $input = $request->all();

        if($request->hasFile('photo') && $request->file('photo')->isValid()) {
            $file = $request->file('photo');
            $filename = time().'.'.$file->getClientOriginalExtension();
            $file->move('images', $filename);
        }

        // Convert to uppercase
        foreach($input as $key => $string) {
            $input[$key] = strtoupper($input[$key]);
        }
        // save data
        $app = Application::create($input);
        // Attach photo
        $app->photo()->create(['filename' => $filename]);

        // generate ref no
        $app->ref_no = sprintf('%04d', $app->id);
        $app->save();

        // add verification items
        $vitems = VerificationItem::all();
        foreach($vitems as $vitem) {
            $vitem->applications()->attach($app->id, ['status' => 0, 'created_at' => Carbon::now()]);
        }

        return redirect()->route('applications.show', $app->id);
        } else {
             return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Application  $application
     * @return \Illuminate\Http\Response
     */
    public function show(Application $application)
    {
        if(Gate::allows('application_access')) {
            $types = Type::all();
            $session = Session::current();
            if($application->hasApplicant()){
                $apt = $application->applicant;
            } else {
                $apt = "";
            }
            // if this application hasn't a an applicant create if status === 1
            if($application->status === 1 && $application->hasApplicant() === false){
                $application->applicants()->create();
            }

            return view('admin.applications.view', compact('application', 'types', 'session', 'apt'));
        } else {
            return back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Application  $application
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Gate::allows('application_access')){
            $current = Session::current();
            $app = $current->applications->find($id);
            if($current){
                return view('admin.applications.edit', compact('current', 'app'));
            }
        } else {
            return back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Application  $application
     * @return \Illuminate\Http\Response
     */
    public function update(ApplicationUpdateRequest $request, $id)
    {
        if(Gate::allows('application_access')) {
            $session = Session::findOrFail($request->session_id);
            $app = $session->applications->find($id);
            // Check birthday date
        if(!empty($request->dob)) {
            $dob = str_replace('/', '-', $request->dob);
            $dob = date('Y-m-d', strtotime($dob));

            $dob_min = date('Y-m-d', strtotime(str_replace('/', '-', $session->birth_min)));
            $dob_max = date('Y-m-d', strtotime(str_replace('/', '-', $session->birth_max)));

            if(Carbon::parse($dob)->lessThan(Carbon::parse($dob_min)) || Carbon::parse($dob)->greaterThan(Carbon::parse($dob_max))){
                return back();
            }
        }

        // upload photo
        $input = $request->all();

        if($request->hasFile('photo') && $request->file('photo')->isValid()) {
             // check a photo has before
            if($app->hasPhoto()) {
                if(file_exists(public_path().$app->photo->filename)){
                    unlink(public_path().$app->photo->filename);
                }
                $app->photo->delete();
            }


            $file = $request->file('photo');
            $filename = time().'.'.$file->getClientOriginalExtension();
            $file->move('images', $filename);

            // Attach new photo
            $app->photo()->create(['filename' => $filename]);
        }

        // Convert to uppercase
        foreach($input as $key => $string) {
            $input[$key] = strtoupper($input[$key]);
        }
        // save data
        $app->update($input);

        return redirect()->route('applications.show', $app->id);
        } else {
             return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Application  $application
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Gate::allows('super_access')){
            $app = Application::findOrFail($id);

            if($app->hasPhoto() && file_exists(public_path().$app->photo->filename)){
                unlink(public_path().$app->photo->filename);
                $app->photo->delete();
            }

            $app->delete();
            return redirect()->route('applications.index');
        }
    }

    public function setStat(Request $request)
    {
        $app = Application::findOrFail($request->id);
        $status = (int)$request->status;
        if($status === 1 && $app->hasApplicant() === false){
            // create new one
            $apt = $app->applicant()->create();

            // all tests
            $tests = Test::all();
            if(count($tests) > 0) {
                foreach($tests as $test) {
                    $test->applicants()->attach($apt->id);
                }
            }
            $app->setStatus($status);
        } else {
            $app->applicant->delete();
            $app->setStatus($status);
        }

        return back();
    }

    public function verify(Request $request)
    {
        $vitem = VerificationItem::findOrFail($request->id);
        $vitem->applications()->updateExistingPivot($request->app, ['status' => $request->status, 'updated_at' => Carbon::now(), 'approvedby' => Auth::id()]);
    }

    public function addItem(Request $request)
    {
       if(Gate::allows('verification_access')) {
            $app = Application::findOrFail($request->appId);
            $app->verificationItems()->attach($request->itemId, ['status' => 0, 'approvedby' => Auth::id(), 'created_at' => Carbon::now()]);
            return back();
       } else {
           return back();
       }
    }

    public function updateRemark(Request $request){
        $app = Application::findOrFail($request->app);
        $app->verificationItems()->updateExistingPivot($request->item, ['remark' => $request->remark, 'updated_at' => Carbon::now()]);
        return $request->remark;
    }

    public function updateRemarkStatus(Request $request)
    {
        $app = Application::findOrFail($request->app);
        $app->verificationItems()->updateExistingPivot($request->item, ['remark_status' => $request->status, 'updated_at' => Carbon::now()]);
        return $request->status;
    }

    public function print($id)
    {
        if(Gate::allows('application_access')) {
            $session = Session::current();
            $app = $session->applications->find($id);
            $form = $session->form();

            if($form && file_exists(public_path().$form->filename)) {
                // initiate FPDI
                $pdf = new Fpdi('P','mm','A4');
                // add a page
                $pdf->AddPage();
                // set the source file
                $pdf->setSourceFile(public_path().$form->filename);
                // import page 1
                $tplId = $pdf->importPage(1);
                // use the imported page and place it at point 10,10 with a width of 100 mm
                $pdf->useTemplate($tplId, 5, 5, 200);

                // second step
                $pdf->SetFont('Helvetica');
                $pdf->setFontSize(10.5);
                $pdf->SetTextColor(0, 0, 0);

                $pdf->SetXY(38, 55.3);
                $pdf->Write(0, $app->ref_no);

                $pdf->SetXY(42, 63.2);
                $pdf->Write(0, $app->refno());

                //insert image
                $image = asset($app->photo->filename);
                $pdf->Image($image,167.5,49,25,33);

                $pdf->setFontSize(8);

                $pdf->SetXY(103, 88);
                $pdf->Write(0, $app->name);

                // splitting dob
                $dob_day = date("d", strtotime(str_replace('/', '-', $app->dob)));
                $dob_month = date("m", strtotime(str_replace('/', '-', $app->dob)));
                $dob_yr = date("Y", strtotime(str_replace('/', '-', $app->dob)));

                // writing in pdf
                $pdf->SetXY(105, 124.7);
                $pdf->Write(0, $dob_day);

                $pdf->SetXY(112, 124.7);
                $pdf->Write(0, $dob_month);

                $pdf->SetXY(120, 124.7);
                $pdf->Write(0, $dob_yr);

                $pdf->SetXY(103, 139);
                $pdf->Write(0, $app->dob_details);

                $pdf->SetXY(34, 186);
                $pdf->Write(0, $app->district);

                $pdf->SetXY(90, 186);
                $pdf->Write(0, $app->province);

                $pdf->SetXY(153, 186);
                $pdf->Write(0, $app->village);

                $pdf->SetXY(103, 195);
                $pdf->Write(0, $app->mobile);

                $pdf->SetXY(103, 202.5);
                $pdf->SetFontSpacing(12);
                $pdf->Write(0, $app->landphone_std);

                $pdf->SetXY(133, 202.5);
                $pdf->SetFontSpacing(11);
                $pdf->Write(0, $app->landphone);

                // set back letter spacing
                $pdf->SetFontSpacing(0);

                $pdf->SetXY(103, 211);
                $pdf->Write(0, $app->name_of_madrassa);

                $pdf->SetXY(103, 218);
                $pdf->Write(0, $app->madrassa_completed_class);

                $pdf->SetXY(103, 227);
                $pdf->Write(0, $app->madrassa_number);

                // add second page
                $pdf->AddPage();
                // set the source file
                $pdf->setSourceFile(public_path().$form->filename);
                // import page 2
                $tplIdx = $pdf->importPage(2);
                // use the imported page and place it at position 10,10 with a width of 100 mm
                $pdf->useTemplate($tplIdx, 5, 5, 200);


                $pdf->Output("D", 'AF-'.$app->refno().'.pdf');
                exit;
            } else {
                return back();
            }
        } else {
            return back();
        }
    }

    public function search(Request $request)
    {
        if(Gate::allows('application_access')) {
            $q = $request->q;
            $current = Session::current();
            $apps = $current->applications()->where('ref_no', 'LIKE', "%$q%")->get();
            return view('admin.applications.search', compact('apps', 'q'));
        } else {
            return back();
        }
    }
}
