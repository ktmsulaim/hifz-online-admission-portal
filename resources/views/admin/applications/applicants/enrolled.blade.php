@extends('layouts.admin')

@section('page_title')
    Applicants
@endsection

@section('breadcrumb')
    <li><a href="#">Applicants</a></li>
    <li class="active"><a href="{{route('enrolled')}}">Enrolled</a></li>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Enrolled applicants</h3>
                <div class="box-tools">
                    <form action="{{route('updateResult')}}" method="post">
                        @csrf
                        <input type="hidden" name="result" value="{{$result->id}}">
                        <input type="hidden" name="status" value="{{$result->status === 1 ? 0 : 1}}">

                        @if($result->status === 0)
                            <button data-toggle="tooltip" data-original-title="Publish result" class="btn btn-sm btn-success" type="submit">Publish</button>
                        @else
                            <button data-toggle="tooltip" data-original-title="Un publish result" class="btn btn-sm btn-danger" type="submit">Un publish</button>
                        @endif
                    </form>
                </div>
            </div>
            <div  class="box-body">
                @if (count($applicants) > 0)
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Rank</th>
                                <th>Ref.no</th>
                                <th>Name</th>
                                <th>Section</th>
                                @foreach ($current->tests as $testHead)
                                    <th>{{$testHead->name}}</th>
                                @endforeach
                                <th>Total</th>
                                <th>&nbsp;%&nbsp;</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($applicants as $key => $apt)
                                <tr>
                                    <td>{{$apt->rank}}</td>
                                    <td><a href="{{route('applications.show', $apt->application->id)}}">{{$apt->application->refno()}}</a></td>
                                    <td>{{$apt->application->name}}</td>
                                    <td>{{$apt->hasSection() ? $apt->getSection()->name : 'No section'}}</td>
                                    @foreach ($apt->tests as $test)
                                        <td>{{$test->pivot->mark}}</td>
                                    @endforeach
                                    <td>{{$apt->marks}}</td>
                                    <td>{{($apt->marks / $max) * 100}}%</td>
                                    <td>
                                        <form action="{{route('rfl')}}" method="post">
                                            @csrf
                                            @method('POST')
                                            <input type="hidden" name="result_id" value="{{$apt->result->first()->id}}">
                                            <input type="hidden" name="apt_id" value="{{$apt->id}}">
                                            <button data-toggle="tooltip" data-original-title="Remove from list" class="btn-simple" type="submit"><i class="text-danger fa fa-times-circle-o fa-fw"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @else
                    <p class="text-muted text-center"><i class="fa fa-frown-o fa-fw"></i> No applicants found!</p>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection