<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Application;
use App\Session;
use App\VerificationItem;
use Illuminate\Http\Resources\Json\Resource;

Auth::routes();

Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', 'FrontController@index')->name('welcome');

Route::get('/result', 'FrontController@result')->name('result');

Route::group(['prefix' => 'admin', 'middleware' => ['admin']], function () {
    Route::get('/', 'AdminHomeController@index')->name('admin');
    Route::get('/search/user', 'UserController@search');
    Route::get('/settings', 'AdminHomeController@settings')->name('settings');
    Route::post('/setstat', 'AdminApplicationController@setStat')->name('setStat');
    Route::post('/verification/item/update', 'AdminApplicationController@verify');
    Route::post('/addItem', 'AdminApplicationController@addItem');
    Route::post('/updateRemark', 'AdminApplicationController@updateRemark');
    Route::post('/remark/updateStatus', 'AdminApplicationController@updateRemarkStatus');
    Route::get('/application/{id}/print', 'AdminApplicationController@print');
    Route::get('applicants/attendance', 'ApplicantController@attendance')->name('attendance');
    Route::post('applicant/attendance/mark', 'ApplicantController@mark')->name('markAppearance');
    Route::get('/markentry', 'ApplicantController@markentry')->name('markentry');
    Route::get('/apt', 'ApplicantController@findapt')->name('findapt');
    Route::post('/apt/updateMarks', 'ApplicantController@updateMarks')->name('updateMarks');
    Route::get('/applicants/waitingList', 'ApplicantController@wlist')->name('waitingList');
    Route::post('/applicants/addToEnrolled', 'ApplicantController@addToEnrolled')->name('addToEnrolled');
    Route::get('/applicants/enrolled', 'ApplicantController@enrolled')->name('enrolled');
    Route::post('/applicants/removeFromList', 'ApplicantController@removeFromList')->name('rfl');
    Route::post('/result/updateStatus', 'ResultController@updateStatus')->name('updateResult');
    Route::post('/settings/about/update', 'AdminHomeController@updateAbout')->name('updateAbout');
    Route::get('/settings/instructions', 'AdminHomeController@instructions')->name('instructions');
    Route::post('/settings/instructions/update', 'AdminHomeController@updateInst')->name('updateInst');
    Route::get('/search/application', 'AdminApplicationController@search')->name('searchApplications');


    Route::resource('users', 'UserController');
    Route::resource('session', 'SessionController');
    Route::resource('applications', 'AdminApplicationController');
    Route::resource('types', 'TypeController');
    Route::resource('vitems', 'VerificationItemController');
    Route::resource('documents', 'DocumentController');
    Route::resource('sections', 'SectionController');
    Route::resource('tests', 'TestController');

});

Route::group(['prefix' => 'client', 'middleware' => ['client']], function(){
    Route::get('/', 'ClientHomeController@index')->name('client');
    Route::get('/profile', 'ClientHomeController@profile')->name('profile');
    Route::get('/profile/changepass', 'ClientHomeController@changepass');
    Route::post('/profile/password/change', 'ClientHomeController@changePassword')->name('changePassword');
    Route::get('/about', 'ClientHomeController@about')->name('about');
    Route::post('/section/set', 'ClientHomeController@setSection')->name('setSection');
    Route::post('/section/change', 'ClientHomeController@changeSection')->name('changeSection');
    Route::get('/application/{id}/print', 'ApplicationController@print');
    Route::get('/application/{id}/hallticket', 'ApplicationController@hallticket')->name('getHallTicket');

    Route::resource('application', 'ApplicationController');
});

Route::post('/calcage', 'HomeController@calcage');

Route::get('/test', function(){
    // // Authorisation details.
	// $username = "ktmsulaim@gmail.com";
	// $hash = "21aca9c5fb2b4bbc99a5c80d7f899dd6519657af9be6737775d4958fbfb51a7e";

	// // You shouldn't need to change anything here.
	// $data = "username=ktmsulaim@gmail.com&hash=".$hash;
	// $ch = curl_init('http://api.textlocal.in/balance/?');
	// curl_setopt($ch, CURLOPT_POST, true);
	// curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	// curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	// $credits = curl_exec($ch);
	// // This is the number of credits you have left
    // curl_close($ch);

    // $app = Application::find(1);
    // $ids= [];
    // foreach($app->verificationItems as $vitem) {
    //    array_push($ids, $vitem->id);
    // }

    // $items = VerificationItem::all();
    // foreach($items as $item) {
    //     if(in_array($item->id, $ids) === false){
    //         echo $item->id." Is not in the list <br>";
    //     }
    // }

    // print_r($ids);
    // dd($app->verificationItems);

    // App\Section::resetToken(1);
    // App\Applicant::updateRank();
});
