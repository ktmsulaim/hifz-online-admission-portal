<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'name' => 'admin',
            'email' => 'admin@test.com',
            'password' => bcrypt('admin123')
        ]);

        $helpdesk = User::create([
            'name' => 'helpdesk',
            'email' => 'helpdesk@dhic.in',
            'password' => bcrypt('helpdesk')
        ]);

        $test = User::create([
            'name' => 'test',
            'email' => 'test@test.com',
            'password' => bcrypt('test123')
        ]);

        $admin->role()->attach(1);
        $helpdesk->role()->attach(2);
        $test->role()->attach(4);
    }
}
