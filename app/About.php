<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    protected $fillable = [
        'name',
        'address',
        'description',
        'contact',
        'contact2',
        'email',
        'website'
    ];
}
