@extends('layouts.admin')

@section('page_title')
    <h1>
        Sessions
    </h1>
@endsection

@section('breadcrumb')
    <li class="active"><a href="{{route('session.index')}}">Sessions</a></li>
@endsection


@section('content')
    <div class="row">
        <div class="col-sm-12 col-xs-12 col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">All sessions</h3>
                    <div class="box-tools">
                        <a href="{{route('session.create')}}"><i class="fa fa-plus fa-fw"></i> New</a>
                    </div>
                </div>
                <div class="box-body no-padding">
                    @if (count($sessions) > 0)
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>From</th>
                                    <th>End</th>
                                    <th>Max.Count</th>
                                    <th>Birth min</th>
                                    <th>Birth max</th>
                                    <th>Status</th>
                                    <th width="80">Current</th>
                                    <th width="50"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($sessions as $session)
                                    <tr>
                                        <td>{{$session->id}}</td>
                                        <td>{{$session->name}}</td>
                                        <td>{{$session->from}}</td>
                                        <td>{{$session->to}}</td>
                                        <td>{{$session->max_count}}</td>
                                        <td>{{$session->birth_min}}</td>
                                        <td>{{$session->birth_max}}</td>
                                        <td>{!! $session->status == 1 ? '<span class="label label-success">ON</span>' : '<span class="label label-danger">OFF</span>' !!}</td>
                                        <td class="text-center">{!! $session->is_current == 1 ? '<i class="fa fa-check-circle-o fa-fw text-red"></i>' : '' !!}</td>
                                        <td><a href="{{route('session.edit', $session->id)}}"><i class="fa fa-edit fa-fw"></i></a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                    @else
                        <p class="text-muted text-center"><i class="fa fa-frown-o fa-fw"></i> No session found! create <a href="{{route('session.create')}}">new</a></p>
                    @endif
                </div>
                <div class="box-footer">
                    {{$sessions->links()}}
                </div>
            </div>
        </div>
    </div>
@endsection
