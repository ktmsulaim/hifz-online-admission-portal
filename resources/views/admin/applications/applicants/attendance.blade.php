@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" type="text/css" href="{{asset('css/dataTables.bootstrap.min.css')}}">
@endsection

@section('page_title')
    <h1>Attendance</h1>
@endsection

@section('breadcrumb')
    <li><a href="#">Applicants</a></li>
    <li class="active"><a href="{{route('attendance')}}">Attendance</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg 12-col-md-12 col-sm-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Applicants</h3>
                </div>
                <div class="box-body">
                    @if (count($applicants) > 0)
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Ref.no</th>
                                    <th>Name</th>
                                    <th>Section</th>
                                    <th width="180">Status</th>
                                    <th width="50"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($applicants as $key => $applicant)
                                <tr class="{{$applicant->hasSection() ? '' : 'bg-danger'}}">
                                    <td>{{$key + 1}}</td>
                                    <td><a href="{{route('applications.show', $applicant->application->id)}}">{{$applicant->application->refno()}}</a></td>
                                    <td>{{$applicant->application->name}}</td>
                                    <td>{{$applicant->hasSection() ? $applicant->getSection()->name : 'No section'}}</td>
                                    <td>{!! $applicant->appearance() ? '<span class="label label-success" style="margin-right:5px">Appeared</span>'.date('F d, g:i A', strtotime($applicant->getSection()->pivot->appeartime)) : '<span class="label label-danger">Not appeared</span>' !!}</td>
                                    <td>
                                        @if ($applicant->hasSection())
                                            <form action="{{route('markAppearance')}}" method="post">
                                                @csrf
                                                @method('POST')
                                                <input type="hidden" name="applicant_id" value="{{$applicant->id}}">
                                                <input type="hidden" name="section_id" value="{{$applicant->getSection()->id}}">

                                                @if ($applicant->appearance())
                                                    <input type="hidden" name="status" value="0">

                                                @else
                                                    <input type="hidden" name="status" value="1">
                                                @endif

                                                <button data-toggle="tooltip" data-original-title="{{$applicant->appearance() ? 'Mark as not appeared' : 'Mark as appreared'}}" class="btn-simple" type="submit">{!! $applicant->appearance() ? '<i class="fa fa-times-circle-o fa-fw text-danger"></i>' : '<i class="fa fa-check-circle fa-fw text-success"></i>' !!}</button>
                                            </form>

                                        @else
                                            <button class="btn-simple" type="button"><i class="fa fa-chain-broken fa-fw"></i></button>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        <p class="text-muted text-center">No applicants found!</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script type="text/javascript" charset="utf8" src="{{asset('js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" charset="utf8" src="{{asset('js/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>

<script>
    $(function(){
        $('.table').DataTable({
            dom: 'Bfrtip',
        buttons: [
            'print'
        ]
        });
    });
</script>
@endsection
