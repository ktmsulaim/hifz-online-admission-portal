@extends('layouts.admin')

@section('page_title')
    <h1>
        Dashboard
        <small>Darul Hasanath Hifz Al Quran college | Admission portal</small>
    </h1>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="small-box bg-aqua">
                <div class="inner">
                  <h3>{{$users}}</h3>

                  <p>User Registrations</p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
                <a href="{{route('users.index')}}" class="small-box-footer">
                  More info <i class="fa fa-arrow-circle-right"></i>
                </a>
              </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="small-box bg-green">
                <div class="inner">
                  <h3>{{count($current->applications)}}</h3>

                  <p>Applications</p>
                </div>
                <div class="icon">
                  <i class="fa fa-list-ul"></i>
                </div>
                <a @can('application_access') href="{{route('applications.index')}}" @endcan class="small-box-footer">
                  More info <i class="fa fa-arrow-circle-right"></i>
                </a>
              </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>{{$visitors}}</h3>

                    <p>Unique Visitors</p>
                </div>
                <div class="icon">
                    <i class="ion ion-pie-graph"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h5 style="margin-bottom:25px">{{$current ? $current->name : 'No session'}}</h5>

                    <p>Current session</p>
                </div>
                <div class="icon">
                    <i class="fa fa-calendar fa-fw"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
    </div>
@endsection
