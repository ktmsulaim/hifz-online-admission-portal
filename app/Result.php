<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    protected $fillable = ['session_id', 'name', 'status'];

    public function session()
    {
        return $this->belongsTo('App\Session');
    }

    public function applicants()
    {
        return $this->belongsToMany('App\Applicant');
    }

    public function setStatus($status)
    {
        $this->status = $status;
        $this->save();
    }
}
