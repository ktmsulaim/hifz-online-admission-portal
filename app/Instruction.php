<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Instruction extends Model
{
    protected $fillable = ['session_id', 'data'];

    public function session()
    {
        return $this->belongsTo('App\Session');
    }
}
