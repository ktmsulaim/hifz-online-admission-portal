<?php

namespace App\Http\Controllers;

use App\About;
use App\Application;
use App\Http\Requests\ChangePasswordRequest;
use App\Section;
use App\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ClientHomeController extends Controller
{
    public function index()
    {
        $current = Session::current();
        $inst = $current->instruction;
        return view('client.index', compact('current', 'inst'));
    }

    public function profile()
    {
        return view('client.profile');
    }

    public function changepass() {
        return view('client.changepass');
    }

    public function changePassword(ChangePasswordRequest $request)
    {
        $user = Auth::user();
        $user->update(['password' => bcrypt($request->password)]);
        auth()->logout();
        return redirect()->route('login')->with('changed', 'The password has been changed!');
    }

    public function setSection(Request $request)
    {
        $app = Application::findOrFail($request->app);
        $apt = $app->applicant;
        $apt->section()->attach($request->input('section'));
        $section = $request->input('section');
        // $apt->section()->updateExistingPivot($request->input('section'), ['token' => $apt->id]);

        // token ):
        Section::resetToken($section);
        return back();
    }

    public function changeSection(Request $request)
    {
        $app = Application::findOrFail($request->app);
        $apt = $app->applicant;
        $apt->section()->sync($request->input('section'));
        Section::resetToken($request->input('section'));
        return back();
    }

    public function about()
    {
        $about = About::first();
        if($about) {
            return view('client.about', compact('about'));
        } else {
            return back();
        }
    }
}
