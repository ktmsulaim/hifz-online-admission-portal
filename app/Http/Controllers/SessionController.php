<?php

namespace App\Http\Controllers;

use App\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
class SessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       if(Gate::allows('settings_access')){
            $sessions = Session::paginate(10);
            return view('admin.sessions.index', compact('sessions'));
       } else {
           return back();
       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Gate::allows('settings_access')){
            return view('admin.sessions.create');
        } else {
            return back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Gate::allows('settings_access')) {
            $session = Session::create($request->all());

            // if another session has current change to this
            if($session->is_current == 1){
                // get all other current sessions
                $current = Session::where('is_current', '=', 1)->where('id', '!=', $session->id)->get();
                if(count($current) > 0){
                    foreach($current as $s){
                        $s->is_current = 0;
                        $s->save();
                    }
                }
            }

            return redirect()->route('session.index');
        } else {
            return back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Session  $session
     * @return \Illuminate\Http\Response
     */
    public function show(Session $session)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Session  $session
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Gate::allows('settings_access')) {
            $session = Session::findOrFail($id);
            return view('admin.sessions.edit', compact('session'));
        } else {
            return back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Session  $session
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Gate::allows('settings_access')) {
            $session = Session::findOrFail($id);
            $session->update($request->all());

            // if another session has current change to this
            if($session->is_current == 1){
                // get all other current sessions
                $current = Session::where('is_current', '=', 1)->where('id', '!=', $session->id)->get();
                if(count($current) > 0){
                    foreach($current as $s){
                        $s->is_current = 0;
                        $s->save();
                    }
                }
            }

            return redirect()->route('session.index');
        } else {
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Session  $session
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Gate::allows('settings_access')) {
            $session = Session::findOrFail($id);
            $session->delete();

            return redirect()->route('session.index');
        } else {
            return back();
        }
    }
}
