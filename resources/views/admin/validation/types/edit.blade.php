@extends('layouts.admin')

@section('page_title')
    <h1>Types</h1>
@endsection

@section('breadcrumb')
    <li><a href="{{route('types.index')}}">Types</a></li>
    <li class="active"><a href="{{route('types.edit', $t->id)}}">Edit</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Items in {{$t->name}}</h3>
                </div>
                <div class="box-body">

                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Created on</th>
                                    <th width="50"></th>
                                    <th width="50"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (count($t->verificationItems) > 0)
                                    @foreach ($t->verificationItems as $key => $vitem)
                                    <tr>
                                        <td>{{$key + 1}}</td>
                                        <td>{{$vitem->name}}</td>
                                        <td>{{date('F d, Y h:i A', strtotime($vitem->created_at))}}</td>
                                        <td><a class="btn btn-link btn-sm" href="{{route('vitems.edit', $vitem->id)}}"><i class="fa fa-edit fa-fw"></i></a></td>
                                        <td>
                                            <form action="{{route('vitems.destroy', $vitem->id)}}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button class="btn btn-link btn-sm" type="submit"><i class="fa fa-trash fa-fw"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                @endif
                                <tr>
                                    <td>#</td>
                                    <td colspan="3"><input form="item" type="text" name="name" id="name" placeholder="Name of item" class="form-control" required></td>
                                    <td><button class="btn btn-primary" form="item" type="submit"><i class="fa fa-plus fa-fw"></i> Create</button></td>
                                </tr>
                            </tbody>
                        </table>
                        <form id="item" action="{{route('vitems.store')}}" method="post">
                            @csrf
                            @method('POST')
                            <input type="hidden" name="type_id" value="{{$t->id}}">
                        </form>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit {{$t->name}}</h3>
                </div>
                <div class="box-body">
                    <form action="{{route('types.update', $t->id)}}" method="post">
                        @csrf
                        @method('PATCH')

                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" name="name" id="name" class="form-control" value="{{old('name', $t->name)}}" required>
                        </div>

                        <div class="text-right">
                            <button class="btn btn-primary" type="submit"><i class="fa fa-send fa-fw"></i> Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
@endsection
