@extends('layouts.admin')

@section('page_title')
    <h1>Applicants</h1>
@endsection

@section('breadcrumb')
    <li><a href="#">Applicants</a></li>
    <li class="active"><a href="{{route('waitingList')}}">Waiting list</a></li>
@endsection

@section('content')
    @if (session('exceeds'))
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Error!</h4>
                    {{session('exceeds')}}
                  </div>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Applicants in waiting list <small>{{$current->hasResult() ? $current->avail().' seats left' : ''}}</small></h3>
                    <div class="box-tools">
                        <form id="add" action="{{route('addToEnrolled')}}" method="post">
                            @csrf
                            @method('POST')
                            <button class="btn btn-box-tool" type="submit">Add to enrolled</button>
                        </form>
                    </div>
                </div>
                <div  class="box-body">
                    <p class="selected"></p>
                    @if (count($applicants) > 0)
                        <table class="table">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" id="checkAll"></th>
                                    <th>Rank</th>
                                    <th>Ref.no</th>
                                    <th>Name</th>
                                    <th>Section</th>
                                    @foreach ($current->tests as $testHead)
                                        <th>{{$testHead->name}}</th>
                                    @endforeach
                                    <th>Total</th>
                                    <th>&nbsp;%&nbsp;</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($applicants as $key => $apt)
                                    @php
                                        if($apt->enrolled()){
                                            continue;
                                        }
                                    @endphp
                                    <tr class="{{$apt->passed() ? '' : 'bg-danger'}}">
                                        <td><input form="add" type="checkbox" class="check" name="ids[]" id="id{{$key + 1}}" value="{{$apt->id}}" {{$apt->passed() ? '' : 'disabled'}}></td>
                                        <td>{{$apt->rank}}</td>
                                        <td><a href="{{route('applications.show', $apt->application->id)}}">{{$apt->application->refno()}}</a></td>
                                        <td>{{$apt->application->name}}</td>
                                        <td>{{$apt->hasSection() ? $apt->getSection()->name : 'No section'}}</td>
                                        @foreach ($apt->tests as $test)
                                            <td>{{$test->pivot->mark}}</td>
                                        @endforeach
                                        <td>{{$apt->marks}}</td>
                                        <td>{{($apt->marks / $max) * 100}}%</td>
                                        <td>{!! $apt->passed() ? '<span class="label label-success">Passed</span>' : '<span class="label label-danger">Not passed</span>'  !!}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        <p class="text-muted text-center"><i class="fa fa-frown-o fa-fw"></i> No applicants found!</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(function(){
            function selected(){
                let sel = $('.check:checked').length;
                $('.selected').empty();
                $('.selected').html('The applicants selected <strong>' + sel + '</strong>');
            }

            $('#checkAll').click(function(){
                if($(this).prop('checked') === true) {
                    $('.check').each(function(){
                       if(this.disabled === false){
                            this.checked = true;
                       }
                    });
                } else {
                    $('.check').each(function(){
                        this.checked = false;
                    });
                }
                selected();
            });

            $('.check').change(selected);
        });
    </script>
@endsection
