@extends('layouts.admin')

@section('page_title')
    <h1>
        Documents
    </h1>
@endsection

@section('breadcrumb')
    <li><a href="{{route('documents.index')}}">Documents</a></li>
    <li class="active"><a href="{{route('documents.create')}}">Create</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8 col-lg-8 col-sm-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">New document</h3>
                </div>
                <div class="box-body">
                    <form action="{{route('documents.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('POST')

                        <input type="hidden" name="session_id" value="{{$session->id}}">

                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" name="title" id="title" class="form-control" value="{{old('title')}}" required>
                        </div>

                        <div class="form-group">
                            <label for="details">Details (Optional)</label>
                            <textarea style="resize:none" name="details" id="details" class="form-control" cols="30" rows="5">{{old('details')}}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="type">Type</label>
                            <select name="type" id="type" class="form-control" required>
                                <option value="">Select a document type</option>
                                <option value="1" {{\App\Document::hasForm() === true ? 'disabled' : ''}}>Application form</option>
                                <option value="2" {{\App\Document::hasTicket() === true ? 'disabled' : ''}}>Hall ticket</option>
                                <option value="0">Other</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="filename">File (.pdf)</label>
                            <input type="file" accept=".pdf" name="filename" id="filename" class="form-control" required>
                        </div>

                        <div class="text-right">
                            <button class="btn btn-primary" type="submit"><i class="fa fa-plus fa-fw"></i> Create</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
            @if (count($errors) > 0)
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Error</h3>
                    </div>
                    <div class="box-body">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(function(){
            $('#filename').fileinput({
                maxFileSize: 1024,
                showUpload: false,
                allowedFileExtensions: ['pdf']
            });
        });
    </script>
@endsection
