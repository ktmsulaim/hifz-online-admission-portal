@extends('layouts.admin')

@section('styles')
    <style>
        #setStat{
            display: none;
        }

        .status:hover #setStat{
            display: inline;
        }
    </style>
@endsection
@section('page_title')
    <h1>Applications</h1>
@endsection

@section('breadcrumb')
    <li><a href="{{route('applications.index')}}">Applications</a></li>
    <li class="active"><a href="{{route('applications.show', $application->id)}}">{{$application->refno()}}</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="box box-widget widget-user-2">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-aqua-active">
                  <div class="widget-user-image">
                    <img class="img-circle" width="60" height="60" src="{{$application->hasPhoto() ? asset($application->photo->filename) : asset('img/150.png')}}" alt="{{$application->name}}">
                  </div>
                  <!-- /.widget-user-image -->
                  <h3 class="widget-user-username">{{$application->name}}</h3>
                  <h5 class="widget-user-desc">{{$application->user->name}}</h5>
                </div>
                <div class="box-footer no-padding">
                  <ul class="nav nav-stacked">
                    <li><a><strong>Ref.no: </strong> <span>{{$application->refno()}}</span></a></li>
                    <li><a><strong>DOB: </strong> <span>{{$application->dob}}</span></a></li>
                    <li><a><strong>Age: </strong> <span>{{$application->dob_details}}</span></a></li>
                    <li><a><strong>Completed class: </strong> <span>{{$application->madrassa_completed_class}}</span></a></li>
                    <li class="status">
                        <a class="clearfix"><strong>Status</strong> {!! $application->isVerified() ? '<span class="label label-success rounded">Verified</span>' : '<span class="label label-danger rounded">Not verified</span>' !!}
                            <form id="setStat" class="d-inline pull-right" action="{{action('AdminApplicationController@setStat')}}" method="post">
                                @csrf
                                @method('POST')
                                <input type="hidden" name="id" value="{{$application->id}}">
                                @if ($application->isVerified())
                                    <input type="hidden" name="status" value="0">
                                    <button type="submit" class="badge bg-red no-border"><i class="fa fa-times-circle fa-fw"></i>Not verify</button>
                                @else
                                    <input type="hidden" name="status" value="1">
                                    <button type="submit" class="badge bg-green-active no-border"><i class="fa fa-check-circle-o fa-fw"></i> Verify</button>
                                @endif
                            </form>
                        </a>
                    </li>
                  </ul>
                </div>
              </div>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Personal info</h3>
                    <div class="box-tools">
                        <a href="{{route('applications.edit', $application->id)}}" data-toggle="tooltip" data-original-title="Edit" class="btn btn-box-tool"><i class="fa fa-edit fa-fw"></i></a>
                        @if ($session->form() && file_exists(public_path().$session->form()->filename))
                        <button form="print" data-toggle="tooltip" data-original-title="Print" class="btn btn-box-tool" type="submit"><i class="fa fa-print fa-fw"></i></button>
                        <form id="print" action="{{url('/admin/application/'.$application->id.'/print')}}" target="_blank" method="get">

                        </form>
                        @endif
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-bordered">
                        <tr>
                            <th>District</th>
                            <td>{{$application->district}}</td>
                        </tr>
                        <tr>
                            <th>Province</th>
                            <td>{{$application->province}}</td>
                        </tr>
                        <tr>
                            <th>Phone</th>
                            <td>{{$application->mobile}}</td>
                        </tr>
                        <tr>
                            <th>Landphone</th>
                            <td>{{$application->landphone_std.' '.$application->landphone}}</td>
                        </tr>
                        <tr>
                            <th>Name of madrassa</th>
                            <td>{{$application->name_of_madrassa}}</td>
                        </tr>
                        <tr>
                            <th width="150">Madrassa number</th>
                            <td>{{$application->madrassa_number}}</td>
                        </tr>
                    </table>
                </div>
                <div class="box-footer"></div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Verification</h3>
                </div>
                <div class="box-body">
                    @if (count($types) > 0)
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                          @foreach ($types as $key => $type)
                          <li class="{{$key === 0 ? 'active' : ''}}"><a href="#tab_{{$type->id}}" data-toggle="tab">{{$type->name}}</a></li>
                          @endforeach
                        </ul>
                        <div class="tab-content">
                          @foreach ($types as $key => $type)
                          <div class="tab-pane {{$key === 0 ? 'active' : ''}}" id="tab_{{$type->id}}">
                               @if (count($application->verificationItems) > 0)
                               <ul class="todo-list ui-sortable">
                                @foreach ($application->verificationItems as $vitem)
                                    @if ($vitem->type_id === $type->id)
                                    <li class="clearfix">
                                        <div class="pull-left">
                                            <input id="item{{$vitem->id}}" class="item" type="checkbox" value="{{$vitem->id}}" {{$vitem->status($application->id) ? 'checked' : ''}}>
                                            <span class="text"><label for="item{{$vitem->id}}">{{$vitem->name}}</label></span>
                                            @if ($vitem->status($application->id))
                                            <small class="label label-success"><i class="fa fa-check-circle-o"></i> Verified</small>
                                            @else
                                            <small class="label label-danger"><i class="fa fa-times-circle"></i> Not verified</small>
                                            @endif

                                            @if (!empty($vitem->pivot->remark))
                                            <span title="{{$vitem->pivot->remark_status === 1 ? 'Remark is public' : 'Remark is hidden'}}" data-status = "{{$vitem->pivot->remark_status === 1 ? 0 : 1}}" data-app="{{$application->id}}" data-item="{{$vitem->id}}" class="cursor-pointer remarkStat {{$vitem->pivot->remark_status === 1 ? 'text-success' : 'text-danger'}}"><i class="fa {{$vitem->pivot->remark_status === 1 ? 'fa-eye' : 'fa-eye-slash'}} fa-fw"></i></span>
                                            <span class="remarks">{{$vitem->pivot->remark}}</span>
                                            <span class="editBtn cursor-pointer"><i class="fa fa-edit fa-fw"></i> Edit</span>
                                            @endif
                                        </div>
                                        <div class="d-inline pull-left remark-container" style="display:none">
                                            <div class="navbar-form navbar-left">
                                                <input type="hidden" name="applicationId" class="appid" value="{{$application->id}}">
                                                <input type="hidden" name="itemId" class="itemid" value="{{$vitem->id}}">
                                                <div class="input-group input-group-sm">
                                                    <input type="text" class="form-control" name="remarktext" class="remarktext">
                                                        <span class="input-group-btn">
                                                          <button type="button" class="btn btn-primary btn-flat addBtn">Add</button>
                                                        </span>
                                                  </div>
                                            </div>

                                            <span style="cursor:pointer; margin-top:8px" class="btn btn-danger btn-sm closeBtn"><i class="fa fa-times-circle-o fa-fw"></i> Close</span>
                                        </div>
                                        <span style="cursor:pointer; {{!empty($vitem->pivot->remark) ? 'display:none' : ''}}" class="badge pull-right addRemark"><i class="fa fa-commenting-o fa-fw"></i> Add remark</span>
                                    </li>
                                    @endif
                                @endforeach
                               </ul>
                               @endif

                               @if (count($application->toAdd($type->id)) > 0)
                                @can('verification_access', Auth::user())
                                <hr>
                                <h4>Verification items</h4>
                                    <ul class="todo-list">
                                        @foreach ($application->toAdd($type->id) as $key => $toadd)
                                            @foreach ($toadd as $key => $to)
                                                <li class="clearfix">
                                                    <strong class="pull-left">{{$to}}</strong>
                                                    <form class="d-inline-block form-inline pull-left" action="{{url('/admin/addItem')}}" method="post">
                                                        @csrf
                                                        @method('POST')
                                                        <input type="hidden" class="itemId" value="{{$key}}">
                                                        <input type="hidden" class="appId" value="{{$application->id}}">
                                                        <button class="badge bg-gray no-border" style="margin-left:5px" type="submit">Add to list</button>
                                                    </form>
                                                </li>
                                            @endforeach
                                        @endforeach
                                    </ul>
                                @endcan
                               @endif
                          </div>
                          @endforeach
                          <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                      </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
           <div class="box {{$application->hasApplicant() ? 'box-primary' : 'box-warning'}}">
               <div class="box-header with-border">
                   <h3 class="box-title">Section details</h3>
               </div>
               <div class="box-body">
                @if ($application->hasApplicant())
                    @if ($apt->hasSection())
                        <table class="table table-bordered">
                            <tr>
                                <th width="100">Section</th>
                                <td>{{$apt->section()->first()->name}}</td>
                            </tr>
                            <tr>
                                <th width="100">Timing</th>
                                <td>{{$apt->getSection()->details}}</td>
                            </tr>
                            <tr>
                                <th width="100">Date</th>
                                <td>{{$apt->getSection()->start . ' - '. $apt->getSection()->end}}</td>
                            </tr>
                            <tr>
                                <th width="100">Token</th>
                                <td>{{$apt->getSection()->pivot->token}}</td>
                            </tr>
                            <tr>
                                <th width="100">Appearance</th>
                                <td>{!! $apt->getSection()->pivot->appear === 0 ? '<span class="label label-danger">Not appreared</span>' : '<span class="label label-success">Appreared</span><i style="margin-left: 8px" class="fa fa-clock-o fa-fw"></i> '. date('F d, g:i A', strtotime($apt->getSection()->pivot->appeartime)) !!}</td>
                            </tr>
                        </table>
                    @else
                        <p class="text-muted"><i class="fa fa-frown-o fa-fw"></i> The applicant hasn't set a time to appear in the interview!</p>
                    @endif
                @else
                    <p class="text-muted"><i class="fa fa-frown-o fa-fw"></i> The applicant is not qualified to appear in the interview!</p>
                @endif
               </div>
           </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
           @if ($apt && $apt->hasSection() && $apt->getSection()->pivot->appear === 1)
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">Result</h3>
                </div>
                <div class="box-body">
                    <table class="table table-bordered">
                        <tr>
                            <th>Test</th>
                            <th>Marks</th>
                            <th>Remark</th>
                        </tr>
                        @if (count($apt->tests) > 0)
                            @foreach ($apt->tests as $test)
                            <tr class="{{$test->pivot->mark >= $test->pass_mark ? '' : 'bg-danger'}}">
                                <th width="130">{{$test->name}}</th>
                                <td>{{$test->pivot->mark}}</td>
                                <td>{{$test->pivot->remark}}</td>
                            </tr>
                            @endforeach
                        @endif
                    </table>
                    <ul class="nav nav-stacked">
                        <li><a><strong style="margin-right:8px">Status</strong>
                            @if ($apt->status === 1)
                                    <span class="badge bg-green">Enrolled</span>
                                @elseif($apt->status === 2)
                                    <span class="badge bg-blue">Waiting list</span>
                                @else
                                    <span class="badge bg-red">Not selected</span>
                                @endif
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
           @endif
        </div>
        <div class="clearfix"></div>
    </div>
@endsection

@section('js')
    <script>
        $(function(){
            function updateStatus() {
                var el = $(this);
                var id = parseInt($(this).val());
                var statDom = $(this).parent().find('.label');
                var status = 0;
                if(el.prop('checked')) {
                    status = 1;
                } else {
                    status = 0;
                }
                if(id !== '' && status !== ''){
                    $.ajax({
                        url: '/admin/verification/item/update',
                        method: 'POST',
                        data: {
                            '_token': '{{csrf_token()}}',
                            'id': id,
                            'status': status,
                            'app': {{$application->id}}
                        }
                    }).done(function(data){
                        if(status === 1) {
                            statDom.toggleClass('label-success label-danger');
                            statDom.text('Verified');
                        } else {
                            statDom.toggleClass('label-danger label-success');
                            statDom.text('Not verified');
                        }
                    });
                }
            }

            $('.item').change(updateStatus);

            // add remark DOM
            $('body').on('click', '.addRemark', function(){
                var remarks = $(this).parent().find('.remarks');
                remarks.hide();
                $(this).hide();
                var remarkDOM = $(this).prev('.remark-container');
                remarkDOM.css({'display': 'block'});

                if(remarks !== ''){
                    remarkDOM.find('.remarktext').val(remarks.text());
                }

            });

            $('body').on('click', '.closeBtn', function(){
                var remarkDOM = $(this).parent();
                var text = remarkDOM.find('input[type="text"]').val();
                var pre = remarkDOM.prev().find('.remarks').text();
                remarkDOM.css({'display': 'none'});

                if(text === '' && pre === '') {
                    remarkDOM.next().show();
                } else {
                    remarkDOM.prev().find('.remarks').show();
                    remarkDOM.prev().find('.editBtn').show();
                }

            });

            // adding remarks
            function addRemark(){
                var app = $(this).parent().parent().prev().prev();
                var item = $(this).parent().parent().prev();
                var remarks = $(this).parent().prev();
                var container = $(this).closest('.remark-container');
                var remarkStat = container.prev().find('.remarkStat');

                $.ajax({
                    url: '/admin/updateRemark',
                    method: 'POST',
                    data: {
                        '_token': '{{csrf_token()}}',
                        'app': app.val(),
                        'item': item.val(),
                        'remark': remarks.val()
                    }
                }).done(function(data){
                    container.css({'display': 'none'});
                    remarkStat.hide();
                    if(data === '') {
                        container.parent().append('<span style="cursor:pointer; " class="badge pull-right addRemark"><i class="fa fa-commenting-o fa-fw"></i> Add remark</span>');
                    } else {
                        if(remarkStat.length) {
                            remarkStat.show();
                        } else {
                            container.prev().append('<span title="Remark is hidden" data-status="1" data-app="'+app.val()+'" data-item="'+item.val()+'" class="remarkStat cursor-pointer text-danger"><i class="fa fa-eye-slash fa-fw"></i></span>');
                        }
                        container.prev().append('<span class="remarks">' + data + '</span>');
                        container.prev().append('<span class="editBtn cursor-pointer"><i class="fa fa-edit fa-fw"></i> Edit</span>');
                    }
                });
            }

            $('.addBtn').click(addRemark);

            // edit remark
            $('body').on('click', '.editBtn', function(){
                var remark = $(this).prev();
                var text = remark.text();
                var container = $(this).parent().next();
                remark.hide();
                $(this).hide();
                container.css({'display':'block'});
                container.find('input[type="text"]').val(text);
                container.find('button').text('Update');
            });

            // make remark public or not
            $('body').on('click', '.remarkStat', function(){
                var status = $(this).data('status');
                var app = $(this).data('app');
                var item = $(this).data('item');
                var el = $(this);

                if(parseInt(status) !== '') {
                    $.ajax({
                        url: '/admin/remark/updateStatus',
                        method: 'POST',
                        data: {
                            '_token': '{{csrf_token()}}',
                            'app': app,
                            'item': item,
                            'status': status
                        }
                    }).done(function(data){
                        if(data === 1) {
                            el.find('i').toggleClass('fa-eye fa-eye-slash');
                            el.toggleClass('text-danger text-success');
                            el.data('status', 0);
                            el.attr('title', 'Remark is public');
                        } else {
                            el.find('i').toggleClass('fa-eye fa-eye-slash');
                            el.toggleClass('text-danger text-success');
                            el.data('status', 1);
                            el.attr('title', 'Remark is hidden');
                        }
                    });
                }
            });

        });
    </script>
@endsection
