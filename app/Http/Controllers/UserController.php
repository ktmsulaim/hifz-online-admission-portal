<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Photo;
use App\Policies\UserPolicy;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       if(Gate::allows('users_access')){
            $users = User::paginate(15);
            return view('admin.users.index', compact('users'));
       } else {
           return back();
       }
          
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Gate::allows('users_access')){
            $roles = Role::all();
            return view('admin.users.create', compact('roles'));
        }else{
            return back();
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserCreateRequest $request)
    {
        if(Gate::allows('users_access')){
            $input = $request->all();
            
            if(!empty($request->input('password'))){
                $input['password'] = bcrypt($request->input('password'));
            }

            $user = User::create($input);

            if($request->hasFile('photo')){
                if($request->file('photo')->isValid()){
                    $file = $request->file('photo');
                    $name = time().'.'.$file->getClientOriginalExtension();
                    $file->move('images', $name);
                    $photo = new Photo();
                    $photo->filename = $name;
                    $photo->photoable_id = $user->id;
                    $photo->photoable_type = 'App\User';
                    $photo->save();
                }
            }

            if(!empty($input['role_id'])) {
                $user->role()->attach($input['role_id']);
            }

            return redirect()->route('users.index');

        }else{
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Gate::allows('users_access')){
            $user = User::findOrFail($id);
            return view('admin.users.view', compact('user'));
        }else{
            return back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Gate::allows('users_access')){
            $user = User::findOrFail($id);
            $roles = Role::all();
            return view('admin.users.edit', compact('user', 'roles'));
        }else{
            return back();
        }   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, $id)
    {
        if(Gate::allows('users_access')) {
            if($request->input('password') == '' || empty($request->password)){
                $input = $request->except('password');
            }else{
                $input = $request->all();
                $input['password'] = bcrypt($request->input('password'));
            }
            $user = User::findOrFail($id);
            $user->update($input);


            if($request->hasFile('photo')){
                // if already has a photo ? remove it
                if($user->photo){
                    $old = Photo::findOrFail($user->photo->id);
                    unlink(public_path().$old->filename);
                    $old->delete();
                }

                if($request->file('photo')->isValid()){
                    $file = $request->file('photo');
                    $name = time().'.'.$file->getClientOriginalExtension();
                    $file->move('images', $name);
                    $photo = new Photo();
                    $photo->filename = $name;
                    $photo->photoable_id = $user->id;
                    $photo->photoable_type = 'App\User';
                    $photo->save();
                }
            }

            // Check whether user has a role before
            if($user->hasAnyRole()){
                // retrieve all roles || A user only have 1 role
                // foreach($user->role as $role) {
                //     // If selects one thats already assigned: continue, else delete others
                //     if($input['role_id'] === $role->id){
                //         // do nothing
                //     } else {
                //         // First remove existing roles and later will add out of loop
                //         $user->role()->detach($role->id);
                //     }
                // }

                // sync new role
                $user->role()->sync($input['role_id']);
            } else {
                 // assign new role
                 $user->role()->attach($input['role_id']);
            }

            return redirect()->route('users.show', $user->id);
        } else {
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Gate::allows('users_access')) {
            $user = User::findOrFail($id);

            if($user->photo){
                $photo = Photo::findOrFail($user->photo->id);
                unlink(public_path().$photo->filename);
                $photo->delete();
            }

            $user->delete();
            return redirect()->route('users.index');
        } else {
            return back();
        }
    }


    public function search(Request $request)
    {
        if(Gate::allows('users_access')) {
            $q = $request->q;
            $users = User::where('name', 'LIKE', "%$q%")->where('email', 'LIKE', "%$q%")->get();
            return view('admin.users.search', compact('users', 'q'));
        } else {
            return back();
        }
    }
}
