<?php

namespace App\Http\Controllers;

use App\Session;
use App\Section;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class SectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Gate::allows('super_access')){
            $current = Session::current();
            $sections = $current->sections;
            return view('admin.sections.index', compact('current', 'sections'));
        } else {
            return back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Gate::allows('super_access')){
            $current = Session::current();
            return view('admin.sections.create', compact('current'));
        } else {
            return back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Gate::allows('super_access')){
            Section::create($request->all());
            return redirect()->route('sections.index');
       } else {
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function show(Section $section)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Gate::allows('super_access')){
            $current = Session::current();
            $section = Section::findOrFail($id);
            return view('admin.sections.edit', compact('section', 'current'));
        } else {
            return back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Gate::allows('super_access')){
            $section = Section::findOrFail($id);
            $section->update($request->all());
            return redirect()->route('sections.index');
        } else {
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Gate::allows('super_access')){
            $section = Section::findOrFail($id);
            $section->delete();
            return redirect()->route('sections.index');
        } else {
            return back();
        }
    }
}
