<?php

namespace App\Http\Controllers;

use App\Application;
use App\Http\Requests\ApplicationCreateRequest;
use App\Http\Requests\ApplicationUpdateRequest;
use App\Session;
use App\Type;
use App\VerificationItem;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use setasign\Fpdi\Fpdi;


class ApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $apps = Auth::user()->applications;
        $current = Session::current();
        return view('client.applications.index', compact('apps', 'current'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $current = Session::current();
        if($current && $current->status === 1){
            return view('client.applications.create', compact('current'));
        }else{
            return back();
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ApplicationCreateRequest $request)
    {
        // Check birthday date
        if(!empty($request->dob)) {
            $session = Session::findOrFail($request->session_id);
            $dob = str_replace('/', '-', $request->dob);
            $dob = date('Y-m-d', strtotime($dob));

            $dob_min = date('Y-m-d', strtotime(str_replace('/', '-', $session->birth_min)));
            $dob_max = date('Y-m-d', strtotime(str_replace('/', '-', $session->birth_max)));

            if(Carbon::parse($dob)->lessThan(Carbon::parse($dob_min)) || Carbon::parse($dob)->greaterThan(Carbon::parse($dob_max))){
                return back();
            }
        }

        // upload photo
        $input = $request->all();

        if($request->hasFile('photo') && $request->file('photo')->isValid()) {
            $file = $request->file('photo');
            $filename = time().'.'.$file->getClientOriginalExtension();
            $file->move('images', $filename);
        }

        // Convert to uppercase
        foreach($input as $key => $string) {
            $input[$key] = strtoupper($input[$key]);
        }
        // save data
        $app = Application::create($input);
        // Attach photo
        $app->photo()->create(['filename' => $filename]);

        // generate ref no
        $app->ref_no = sprintf('%04d', $app->id);
        $app->save();

        // add verification items
        $vitems = VerificationItem::all();
        foreach($vitems as $vitem) {
            $vitem->applications()->attach($app->id, ['status' => 0, 'created_at' => Carbon::now()]);
        }

        return redirect()->route('application.index')->with('success', 'The application has been submitted.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Application  $application
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $current = Session::current();
        $application = Auth::user()->applications->find($id);
        if($application) {
            $types = Type::all();
            $sections = $current->sections;
            $apt = $application->applicant;
             // if this application hasn't a an applicant create if status === 1
             if($application->status === 1 && $application->hasApplicant() === false){
                $application->applicant->create();
            }
            return view('client.applications.view', compact('current', 'application', 'types', 'sections', 'apt'));
        } else {
            return back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Application  $application
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $current = Session::current();
        $app = Auth::user()->applications->find($id);

        if(!$app){
            return back();
        }

        return view('client.applications.edit', compact('current', 'app'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Application  $application
     * @return \Illuminate\Http\Response
     */
    public function update(ApplicationUpdateRequest $request, $id)
    {
        $session = Session::findOrFail($request->session_id);
        if($session->status === 1) {
            $app = Auth::user()->applications->find($id);

            if(!$app) {
                return back();
            }

            // Check birthday date
            if(!empty($request->dob)) {
                $dob = str_replace('/', '-', $request->dob);
                $dob = date('Y-m-d', strtotime($dob));

                $dob_min = date('Y-m-d', strtotime(str_replace('/', '-', $session->birth_min)));
                $dob_max = date('Y-m-d', strtotime(str_replace('/', '-', $session->birth_max)));

                if(Carbon::parse($dob)->lessThan(Carbon::parse($dob_min)) || Carbon::parse($dob)->greaterThan(Carbon::parse($dob_max))){
                    return back();
                }
            }

            // upload photo
            $input = $request->all();

            if($request->hasFile('photo') && $request->file('photo')->isValid()) {
                // check a photo has before
                if($app->hasPhoto()) {
                    if(file_exists(public_path().$app->photo->filename)){
                        unlink(public_path().$app->photo->filename);
                    }
                    $app->photo->delete();
                }


                $file = $request->file('photo');
                $filename = time().'.'.$file->getClientOriginalExtension();
                $file->move('images', $filename);

                // Attach new photo
                $app->photo()->create(['filename' => $filename]);
            }

            // Convert to uppercase
            foreach($input as $key => $string) {
                $input[$key] = strtoupper($input[$key]);
            }
            // save data
            $app->update($input);

            return redirect()->route('application.show', $app->id)->with('success', 'The application has been updated!');
        } else {
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Application  $application
     * @return \Illuminate\Http\Response
     */
    public function destroy(Application $application)
    {
        //
    }

    public function print($id)
    {
        $session = Session::current();
        $app = Auth::user()->applications->find($id);
        $form = $session->form();
        if($app) {
            if($form && file_exists(public_path().$form->filename)) {
                // initiate FPDI
                $pdf = new Fpdi('P','mm','A4');
                // add a page
                $pdf->AddPage();
                // set the source file
                $pdf->setSourceFile(public_path().$form->filename);
                // import page 1
                $tplId = $pdf->importPage(1);
                // use the imported page and place it at point 10,10 with a width of 100 mm
                $pdf->useTemplate($tplId, 5, 5, 200);

                // second step
                $pdf->SetFont('Helvetica');
                $pdf->setFontSize(10.5);
                $pdf->SetTextColor(0, 0, 0);

                $pdf->SetXY(38, 55.3);
                $pdf->Write(0, $app->ref_no);

                $pdf->SetXY(42, 63.2);
                $pdf->Write(0, $app->refno());

                //insert image
                $image = asset($app->photo->filename);
                $pdf->Image($image,167.5,49,25,33);

                $pdf->setFontSize(8);

                $pdf->SetXY(103, 88);
                $pdf->Write(0, $app->name);

               // splitting dob
               $dob_day = date("d", strtotime(str_replace('/', '-', $app->dob)));
               $dob_month = date("m", strtotime(str_replace('/', '-', $app->dob)));
               $dob_yr = date("Y", strtotime(str_replace('/', '-', $app->dob)));

                // writing in pdf
                $pdf->SetXY(105, 124.7);
                $pdf->Write(0, $dob_day);

                $pdf->SetXY(112, 124.7);
                $pdf->Write(0, $dob_month);

                $pdf->SetXY(120, 124.7);
                $pdf->Write(0, $dob_yr);

                $pdf->SetXY(103, 139);
                $pdf->Write(0, $app->dob_details);

                $pdf->SetXY(34, 186);
                $pdf->Write(0, $app->district);

                $pdf->SetXY(90, 186);
                $pdf->Write(0, $app->province);

                $pdf->SetXY(153, 186);
                $pdf->Write(0, $app->village);

                $pdf->SetXY(103, 195);
                $pdf->Write(0, $app->mobile);

                $pdf->SetXY(103, 202.5);
                $pdf->SetFontSpacing(12);
                $pdf->Write(0, $app->landphone_std);

                $pdf->SetXY(133, 202.5);
                $pdf->SetFontSpacing(11);
                $pdf->Write(0, $app->landphone);

                // set back letter spacing
                $pdf->SetFontSpacing(0);

                $pdf->SetXY(103, 211);
                $pdf->Write(0, $app->name_of_madrassa);

                $pdf->SetXY(103, 218);
                $pdf->Write(0, $app->madrassa_completed_class);

                $pdf->SetXY(103, 227);
                $pdf->Write(0, $app->madrassa_number);

                // add second page
                $pdf->AddPage();
                // set the source file
                $pdf->setSourceFile(public_path().$form->filename);
                // import page 2
                $tplIdx = $pdf->importPage(2);
                // use the imported page and place it at position 10,10 with a width of 100 mm
                $pdf->useTemplate($tplIdx, 5, 5, 200);


                $pdf->Output("D", 'AF-'.$app->refno().'.pdf');
                exit;
            } else {
                return back();
            }
        } else {
            return back();
        }
    }

    public function hallTicket($id)
    {
        $current = Session::current();
        $app = $current->applications->find($id);
        $ht = $current->hallTicket();

        if($app->applicant->hasSection()) {
            $section = $app->applicant->getSection()->name.' / '.$app->applicant->getSection()->pivot->token;
        } else {
            $section = 'No section';
        }
        if($current->status === 1 && $app) {
            if($ht && file_exists(public_path().$ht->filename)){
                // initiate FPDI
                $pdf = new Fpdi('P','mm','A4');
                // add a page
                $pdf->AddPage();
                // set the source file
                $pdf->setSourceFile(public_path().$ht->filename);
                // import page 1
                $tplIdx = $pdf->importPage(1);
                // use the imported page and place it at position 10,10 with a width of 100 mm
                $pdf->useTemplate($tplIdx, 5, 5, 200);

                // now write some text above the imported page
                $pdf->SetFont('Helvetica');
                $pdf->setFontSize(10.5);
                $pdf->SetTextColor(0, 0, 0);

                $pdf->SetXY(30, 75.8);
                $pdf->Write(0, $app->ref_no);

                $pdf->SetXY(172, 124.7);
                $pdf->Write(0, $app->refno());

                //insert image
                $image = asset($app->photo->filename);
                $pdf->Image($image,159,75,30,38);

                $pdf->setFontSize(8);

                $pdf->SetXY(65, 90);
                $pdf->Write(0, $app->name);

                $pdf->SetXY(65, 124.3);
                $pdf->Write(0, $app->dob);

                $pdf->SetXY(65, 133.05);
                $pdf->Write(0, $app->mobile);

                $pdf->SetXY(65, 143.55);
                $pdf->Write(0, "DARUL HASANATH ISLAMIC COLLEGE");

                $pdf->SetXY(118, 143.55);
                $pdf->Write(0, "| 0497 2797032, +91 9745589708");

                $pdf->SetXY(65, 152.10);
                $pdf->Write(0, "KANNADIPPARAMB");


                $pdf->SetXY(15.5, 185);
                $pdf->Write(0, $section);



                $pdf->Output("D", 'HT-'.$app->refno().'.pdf');
                exit;
            }
        } else {
            return back();
        }
    }
}
