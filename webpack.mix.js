const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
//    .sass('resources/sass/app.scss', 'public/css');

mix.sass('resources/sass/admin.scss', 'public/css/admin_custom.css');
mix.sass('resources/sass/home.scss', 'public/css/home.css');


// mix.styles([
//     'resources/admin/bower_components/bootstrap/dist/css/bootstrap.min.css',
//     'node_modules/bootstrap-fileinput/css/fileinput.min.css',
//     'resources/admin/bower_components/font-awesome/css/font-awesome.min.css',
//     'resources/admin/bower_components/Ionicons/css/ionicons.min.css',
//     'resources/admin/css/AdminLTE.min.css',
//     'resources/admin/css/skins/_all-skins.min.css',
//     'resources/admin/bower_components/morris.js/morris.css',
//     'resources/admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
//     'resources/admin/bower_components/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
//     'resources/admin/css/alt/AdminLTE-fullcalendar.min.css',
//     'resources/admin/bower_components/select2/dist/css/select2.min.css',
//     'resources/admin/bower_components/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',
//     'resources/admin/bower_components/iCheck/square/blue.css',
//     'resources/admin/bower_components/Ionicons/css/ionicons.min.css'
// ], 'public/css/admin.css').scripts([
//     'resources/admin/bower_components/jquery/dist/jquery.min.js',
//     'resources/admin/bower_components/jquery-ui/jquery-ui.min.js',
//     'resources/admin/bower_components/bootstrap/dist/js/bootstrap.min.js',
//     'node_modules/bootstrap-fileinput/js/fileinput.min.js',
//     'resources/admin/bower_components/morris.js/morris.js',
//     'resources/admin/bower_components/moment/min/moment.min.js',
//     'resources/admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
//     'resources/admin/bower_components/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
//     'resources/admin/bower_components/select2/dist/js/select2.full.min.js',
//     'resources/admin/bower_components/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js',
//     'resources/admin/bower_components/jquery-slimscroll/jquery.slimscroll.min.js',
//     'resources/admin/bower_components/iCheck/icheck.min.js',
//     'resources/admin/js/adminlte.min.js'
// ], 'public/js/admin.js');
