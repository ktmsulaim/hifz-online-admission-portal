<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationVerificationItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_verification_item', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('application_id')->unsigned()->index();
            $table->integer('verification_item_id')->unsigned()->index();
            $table->boolean('status')->default(0);
            $table->text('remark')->nullable();
            $table->boolean('remark_status')->default(0);
            $table->integer('approvedby')->unsigned()->index()->nullable();
            $table->timestamps();

            $table->foreign('application_id')->references('id')->on('applications')->onDelete('cascade');
            $table->foreign('verification_item_id')->references('id')->on('verification_items')->onDelete('cascade');
            $table->foreign('approvedby')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_verificationItem');
    }
}
