<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    protected $fillable = [
        'session_id',
        'user_id',
        'ref_no',
        'name',
        'name_of_home',
        'name_of_father',
        'job_of_father',
        'dob',
        'dob_details',
        'dob_hijri',
        'reason_not_father',
        'name_of_guard',
        'relation_of_guard',
        'job_of_guard',
        'name_of_mahall',
        'district',
        'province',
        'village',
        'mobile',
        'landphone_std',
        'landphone',
        'name_of_madrassa',
        'madrassa_completed_class',
        'madrassa_number',
        'name_of_range',
        'date_of_exam',
        'name_of_school',
        'school_completed_class',
        'identification_mark1',
        'identification_mark2',
        'postal_address',
        'post_office',
        'pincode',
        'status'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function session()
    {
        return $this->belongsTo('App\Session');
    }

    public function photo()
    {
        return $this->morphOne('App\Photo', 'photoable');
    }

    public function hasPhoto()
    {
        if($this->photo()->exists()){
            return true;
        }

        false;
    }

    public function isVerified()
    {
        if($this->status === 1) {
            return true;
        }

        return false;
    }

    public function getRefNoAttribute($val)
    {
        if(!empty($val)){
            return sprintf('%04d', $val);
        }
    }

    public function setStatus($status)
    {
        $this->status = $status;
        $this->save();
    }

    public function verificationItems()
    {
        return $this->belongsToMany('App\VerificationItem')->withPivot(['status', 'remark', 'remark_status', 'approvedby', 'created_at', 'updated_at']);
    }

    public function toAdd($type)
    {
        // Total items
        $vitems = VerificationItem::all();
        $full = [];
        $toadd = [];

        foreach($this->verificationItems as $item) {
            array_push($full, $item->id);
        }

        foreach($vitems as $vitem) {
            if(in_array($vitem->id, $full) === false && $vitem->type_id === $type) {
                array_push($toadd, [$vitem->id => $vitem->name]);
            }
        }

        return $toadd;
    }

    public function refno(){
        $session = Session::current();
        if($session){
            $year = substr($session->year, 2, 2);
            return $year.$this->ref_no;
        }
    }

    public function formno(){
        $session = Session::current();
        if($session){
            $year = substr($session->year, 2, 2);
            return $this->ref_no.' / '.$year;
        }
    }

    public function applicant()
    {
        return $this->hasOne('App\Applicant');
    }

    public function hasApplicant()
    {
        if($this->applicant()->exists()){
            return true;
        }

        return false;
    }

}
