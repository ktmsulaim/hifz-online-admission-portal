@extends('layouts.admin')

@section('page_title')
    <h1>Examination</h1>
@endsection

@section('breadcrumb')
    <li><a href="#">Examination</a></li>
    <li><a href="{{route('markentry')}}">Mark entry</a></li>
    <li><a href="#">{{$applicant->application->refno()}}</a></li>
@endsection

@section('content')
    <div class="row">
        @if(session('success'))
        <div class="col-lg-12 col-md-12">
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Success!</h4>
                {{session('success')}}
              </div>
        </div>
        @endif
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="box box-primary">
                <div class="box-body box-profile">
                  <img style="height: 100px" class="profile-user-img img-responsive img-circle" src="{{asset($applicant->application->photo->filename)}}" alt="Applicant photo">
    
                  <h3 class="profile-username text-center">{{$applicant->application->name}}</h3>
    
                  <p data-toggle="tooltip" data-original-title="{{$applicant->application->dob_details}}" class="text-muted text-center"><em>Born on: </em>{{$applicant->application->dob}}</p>
    
                  <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                      <b>Ref.no</b> <a class="pull-right">{{$applicant->application->refno()}}</a>
                    </li>
                    <li class="list-group-item">
                      <b>Section</b> <a class="pull-right">{{$applicant->hasSection() ? $applicant->getSection()->name : 'No section'}}</a>
                    </li>
                    <li class="list-group-item">
                      <b>Serial no</b> <a class="pull-right">{{$applicant->hasSection() ? $applicant->getSection()->pivot->token : 'No number'}}</a>
                    </li>
                  </ul>
                  
                  <div>
                    <p>Not this applicant? go <a class="" href="{{route('markentry')}}">back</a></p>
                  </div>
                </div>
                <!-- /.box-body -->
              </div>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Mark entry</h3>
                </div>
                <div class="box-body">
                    <form action="{{route('updateMarks')}}" method="post" id="markentry">
                        @csrf
                        @method('POST')
                        <input type="hidden" name="applicant" value="{{$applicant->id}}">
                        @if (count($applicant->tests) > 0)
                            @foreach ($applicant->tests as $test)
                                <div class="form-group">
                                    <label for="test{{$test->id}}">{{$test->name}}</label>
                                    <input type="number" min="0" max="{{$test->max_mark}}" class="form-control" name="test{{$test->id}}" id="test{{$test->id}}" value="{{$test->pivot->mark}}" required>
                                    <textarea class="form-control" name="rema{{$test->id}}" id="rema{{$test->id}}" style="resize:none" placeholder="Remarks...." rows="3">{{$test->pivot->remark}}</textarea>
                                </div>
                            @endforeach

                            <div class="form-group row">
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">
                                    <input type="hidden" name="total" value="0">
                                    <span class="btn btn-app">
                                        <span class="h3 totallabel">0</span><br>
                                        <strong>Total</strong>
                                    </span>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">
                                    <span class="btn btn-app">
                                        <span class="h3">{{$max}}</span><br>
                                        <strong>Max.marks</strong>
                                    </span>
                                </div>
                                <div class="collg-4 col-md-4 col-sm-4 col-xs-4"></div>
                            </div>
                            <div style="margin-top:15px" class="text-right"><button class="btn btn-primary btn-sm" type="submit">Submit</button></div>
                        @else
                        <p class="text-muted"><i class="fa fa-frown-o fa-fw"></i> Some thing went wrong!</p>
                        @endif
                    </form>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
@endsection

@section('js')
    <script>
        $(function(){
            let fields = $('input[type="number"]');
            let fieldsArr = Array.from(fields);
            let sum = 0;
            let currentMark = 0;

            updateMark(calcMarks());

           function updateMark(mark) {
               if(mark > 0) {
                    $('.totallabel').empty();
                    $('.totallabel').text(mark);
                    $('input[name="total"]').val(mark);
               }
           }

           function calcMarks(){
               let els = $('input[type="number"]');
               let total = 0;   
               let elArr = Array.from(els);
               for (cur of elArr) {
                   total += parseFloat(cur.value);
               }

               return parseFloat(total);
           }


            $('#markentry').on('change', 'input[type="number"]', function(e){
                updateMark(calcMarks());
            });

        });
    </script>
@endsection