@extends('layouts.admin')

@section('page_title')
    <h1>Verification items</h1>
@endsection

@section('breadcrumb')
    <li><a href="{{route('vitems.index')}}">Verification items</a></li>
    <li class="active"><a href="{{route('vitems.edit', $vitem->id)}}">Edit</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit {{$vitem->name}}</h3>
                </div>
                <div class="box-body">
                    <form action="{{route('vitems.update', $vitem->id)}}" method="post">
                        @csrf
                        @method('PATCH')

                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" name="name" id="name" class="form-control" value="{{old('name', $vitem->name)}}" required>
                        </div>

                        <div class="form-group">
                            <label for="type_id">Type</label>
                            <select name="type_id" id="type_id" class="form-control" required>
                                <option value="">Select a type</option>
                                @if (count($types) > 0)
                                    @foreach ($types as $type)
                                        <option value="{{$type->id}}" {{$vitem->type_id === $type->id ? 'selected' : ''}}>{{$type->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>

                        <div class="text-right">
                            <button class="btn btn-primary" type="submit"><i class="fa fa-send fa-fw"></i> update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection 