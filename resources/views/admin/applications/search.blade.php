@extends('layouts.admin')

@section('page_title')
    <h1>Applications</h1>
@endsection

@section('breadcrumb')
    <li class="active"><a href="{{route('applications.index')}}">Applications</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-lg-12 xol-sm-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Search result of "{{$q}}"</h3>
                    <div class="box-tools">
                        <form action="{{route('searchApplications')}}" method="get">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" name="q" class="form-control pull-right" value="{{old('q')}}" placeholder="Search" required="">
                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="box-body no-padding">
                    @if (count($apps) > 0)
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Image</th>
                                    <th>Ref.No</th>
                                    <th>Name</th>
                                    <th>Province</th>
                                    <th>Phone</th>
                                    <th>DOB</th>
                                    <th>Applied date</th>
                                    <th>Status</th>
                                    <th width="60">View</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($apps as $key => $app)
                                    <tr>
                                        <td>{{$key + 1}}</td>
                                        <td><img width="60" height="60" class="img-circle" src="{{$app->hasPhoto() ? asset($app->photo->filename) : asset('img/150.png')}}" alt="{{$app->name}}"></td>
                                        <td>{{$app->ref_no}}</td>
                                        <td>{{$app->name}}</td>
                                        <td>{{$app->province}}</td>
                                        <td>{{$app->mobile}}</td>
                                        <td>{{$app->dob}}</td>
                                        <td>{{date('F d, Y', strtotime($app->created_at))}}</td>
                                        <td>{!! $app->isVerified() ? '<span class="label label-success">Verified</span>' : '<span class="label label-danger">Not Verified</span>' !!}</td>
                                        <td><a href="{{route('applications.show', $app->id)}}"><i class="fa fa-eye fa-fw"></i></a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                    @else
                        <p class="text-center text-muted">
                            <i class="fa fa-frown-o fa-fw"></i> No applications found!
                        </p>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
