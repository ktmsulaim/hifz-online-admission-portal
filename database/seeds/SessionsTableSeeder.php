<?php

use Illuminate\Database\Seeder;
use App\Session;

class SessionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Session::create([
            'name' => 'Session 2020 - 2021',
            'year' => '2020',
            'from' => '15/03/2020',
            'to' => '30/04/2020',
            'birth_min' => '25/05/2011',
            'birth_max' => '25/05/2012',
            'max_count' => 15,
            'is_current' => 1,
            'status' => 1
        ]);
    }
}
