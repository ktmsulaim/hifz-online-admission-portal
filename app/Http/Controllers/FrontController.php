<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Session;
use App\Visitor;
use Illuminate\Support\Facades\DB;

class FrontController extends Controller
{

    public function index()
    {
        $current = Session::current();
        $ip = getenv('REMOTE_ADDR');

        if(!DB::table('visitors')->where('ip', '=', $ip)->exists()) {
            $visitor = new Visitor();
            $visitor->ip = $ip;
            $visitor->save();
        }

        return view('welcome', compact('current'));
    }

    public function result() {
        $current = Session::current();
        if($current) {
            if($current->hasResult() && $current->results->first()->status === 1) {
                $result =  $current->results->first();
                return view('result', compact('result'));
            } else {
                return redirect()->route('welcome');
            }
        } else {
            return redirect()->route('welcome');
        }
    }

}
