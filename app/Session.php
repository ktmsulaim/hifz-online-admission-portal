<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    protected $fillable = ['name', 'year', 'from', 'to', 'birth_min', 'birth_max', 'max_count', 'is_current', 'status'];

    public static function current()
    {
        return self::where('is_current', '=', 1)->first();
    }

    public function applications()
    {
        return $this->hasMany('App\Application');
    }

    public function documents()
    {
        return $this->hasMany('App\Document');
    }

    public function form()
    {
        if(Document::hasForm()){
            $form = Document::where('type', '=', 1)->first();
            return $form;
        }
    }

    public function hallTicket()
    {
        if(Document::hasTicket()){
            $ht = Document::where('type', '=', 2)->first();
            return $ht;
        }
    }

    public function sections()
    {
        return $this->hasMany('App\Section');
    }

    public function applicants()
    {
        return $this->hasManyThrough('App\Applicant', 'App\Application');
    }

    public function tests()
    {
        return $this->hasMany('App\Test');
    }

    public function results()
    {
        return $this->hasMany('App\Result');
    }

    public function hasResult()
    {
        if(count($this->results) > 0) {
            return true;
        }
        return false;
    }

    public function avail()
    {
        if($this->hasResult()){
            return $this->max_count - count($this->results->first()->applicants);
        }
    }

    public function instruction()
    {
        return $this->hasOne('App\Instruction');
    }
}
