@extends('layouts.client')

@section('page_title')
    <h1>
        Applications
    </h1>
@endsection

@section('breadcrumb')
    <li class="active"><a href="{{route('application.index')}}">Applications</a></li>
@endsection


@section('content')

@if(session('success'))
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Success!</h4>
               {{ session('success') }}
              </div>
        </div>
    </div>
@endif

@if (count($apps) > 0)
    <div class="row">
        @foreach ($apps as $app)
            @if($app->session_id === $current->id)
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="box box-widget widget-user" style="margin-bottom: 15px">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header bg-aqua-active">
                            <h3 class="widget-user-username">{{$app->name}}</h3>
                            <h5 class="widget-user-desc">&nbsp;</h5>
                        </div>
                        <div class="widget-user-image">
                            <img class="img-circle" width="128" height="128" src="{{$app->hasPhoto() ? asset($app->photo->filename) : asset('img/150.png')}}" alt="Application photo">
                        </div>
                        <div class="box-footer">
                            <div class="row">
                            <div class="col-sm-4 col-lg-4 col-xs-4 border-right">
                                <div class="description-block">
                                <h5 class="description-header">Form no</h5>
                                <span class="description-text">{{$app->formno()}}</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4 col-lg-4 col-xs-4 border-right">
                                <div class="description-block">
                                <h5 class="description-header">Ref.No</h5>
                                <span class="description-text">{{$app->refno()}}</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4 col-lg-4 col-xs-4">
                                <div class="description-block">
                                <h5 class="description-header">Status</h5>
                                <span class="description-text">{!! $app->status === 1 ? '<span class="label label-success">Verified</span>' : '<span class="label label-danger">Not verified</span>' !!}</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <div class="box-body">
                            <div class="text-center">
                                <a class="btn btn-primary" href="{{route('application.show', $app->id)}}">View</a>
                            </div>
                        </div>
                        </div>
                </div>
            @endif
        @endforeach

        <div class="clearfix"></div>
    </div>
@else
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <p class="text-center text-muted"><i class="fa fa-frown-o fa-fw"></i> No application found!</p>
        </div>
    </div>
@endif
@endsection
