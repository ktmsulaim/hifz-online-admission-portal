@extends('layouts.admin')

@section('page_title')
    <h1>Examination</h1>
@endsection

@section('breadcrumb')
    <li class="active"><a href="#">Sections</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs 12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">All sections</h3>
                    <div class="box-tools">
                        <a href="{{route('sections.create')}}" class="btn btn-box-tool"><i class="fa fa-plus fa-fw"></i> New</a>
                    </div>
                </div>
                <div class="box-body">
                    @if(count($sections) > 0)
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Details</th>
                                    <th>Max.count</th>
                                    <th data-toggle="tooltip" data-original-title="Total count of applicants">Total</th>
                                    <th>Start</th>
                                    <th>End</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                               @foreach ($sections as $key => $section)
                               <tr>
                                    <td>{{$key + 1}}</td>
                                    <td>{{$section->name}}</td>
                                    <td>{{$section->details}}</td>
                                    <td>{{$section->count}}</td>
                                    <td>{{count($section->applicants)}}</td>
                                    <td>{{$section->start}}</td>
                                    <td>{{$section->end}}</td>
                                    <td><a href="{{route('sections.edit', $section->id)}}"><i class="fa fa-edit fa-fw"></i></a></td>
                                </tr>
                               @endforeach
                            </tbody>
                        </table>
                    @else
                        <p class="text-muted text-center"><i class="fa fa-frown-o fa-fw"></i> No sections found!</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
