<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Darul Hasanath Hifz al Quran college | Admission portal {{date('Y')}}</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

   <!-- Styles -->
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/admin_custom.css')}}">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  {{-- FavIcon --}}
  <link rel="shortcut icon" href="{{asset('img/logo.png')}}" type="image/x-icon">

  @yield('styles')
</head>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="{{route('welcome')}}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>DH</b></span>
      <!-- logo for regular state and mobile devices -->
      <span data-toggle="tooltip" data-original-title="Darul Hasanath" title="Darul Hasanath Hifz Al Qura'n college | Admission portal" class="logo-lg"><b>DH</b>HQC</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="{{Auth::user()->photo ? Auth::user()->photo->filename : asset('img/150.png')}}" class="user-image" alt="User Image">
              <span class="hidden-xs">{{Auth::user()->name}}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="{{Auth::user()->photo ? Auth::user()->photo->filename : asset('img/150.png')}}" class="img-circle" alt="User Image">

                <p>
                  {{Auth::user()->name}}
                  <small>Member since {{date('F Y', strtotime(Auth::user()->created_at))}}</small>
                </p>
              </li>
              <!-- Menu Body -->
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="{{route('logout')}}" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{Auth::user()->photo ? Auth::user()->photo->filename : asset('img/150.png')}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{Auth::user()->name}}</p>
          <a><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li>
            <a href="{{url('/admin')}}">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            </a>
          </li>
          {{-- Users --}}
         @can('users_access', Auth::user())
        <li class="treeview">
          <a href="{{route('users.index')}}">
            <i class="fa fa-users"></i>
            <span>Users</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{route('users.create')}}"><i class="fa fa-circle-o"></i> New user</a></li>
            <li><a href="{{route('users.index')}}"><i class="fa fa-circle-o"></i> All users</a></li>
          </ul>
        </li>
        @endcan

        @can('application_access', Auth::user())
        {{-- applications --}}
        <li class="treeview">
          <a href="{{route('users.index')}}">
            <i class="fa fa-list-ul"></i>
            <span>Applications</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            @can('verification_access', Auth::user())
            <li class="treeview">
                <a href="{{route('users.index')}}">
                    <i class="fa fa-check"></i>
                    <span>Verification</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="{{route('types.index')}}"><i class="fa fa-circle-o"></i> Types</a></li>
                    <li><a href="{{route('vitems.index')}}"><i class="fa fa-circle-o"></i> Verification items</a></li>
                  </ul>
            </li>
            @endcan
            <li><a href="{{route('applications.create')}}"><i class="fa fa-circle-o"></i> Apply now</a></li>
            <li><a href="{{route('applications.index')}}"><i class="fa fa-circle-o"></i> All applications</a></li>
          </ul>
        </li>
        @endcan

        @can('applicants_access', Auth::user())
        {{-- applicants --}}
        <li class="treeview">
          <a href="{{route('users.index')}}">
            <i class="fa fa-child"></i>
            <span>Applicants</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{route('attendance')}}"><i class="fa fa-circle-o"></i> Attendance</a></li>
            <li><a href="{{route('enrolled')}}"><i class="fa fa-circle-o"></i> Enrolled</a></li>
            <li><a href="{{route('waitingList')}}"><i class="fa fa-circle-o"></i> Waiting list</a></li>
          </ul>
        </li>
        @endcan

        @can('examination_access', Auth::user())
        {{-- examination --}}
        <li class="treeview">
          <a href="{{route('users.index')}}">
            <i class="fa fa-pencil"></i>
            <span>Examination</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            @can('super_access', Auth::user())
            <li><a href="{{route('sections.index')}}"><i class="fa fa-circle-o"></i> Sections</a></li>
            <li><a href="{{route('tests.index')}}"><i class="fa fa-circle-o"></i> Criteria</a></li>
            @endcan
            <li><a href="{{route('markentry')}}"><i class="fa fa-circle-o"></i> Mark entry</a></li>
          </ul>
        </li>
        @endcan

        @can('settings_access')
        <li>
            <a href="{{route('settings')}}">
                <i class="fa fa-cog"></i> <span>Settings</span>
            </a>
          </li>
        @endcan
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      @yield('page_title')


      <ol class="breadcrumb">
        <li><a href="{{url('/admin')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        @yield('breadcrumb')
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      @yield('content')

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; {{date('Y')}} <a href="http://sacreationsknr.info">SA Creations</a>.</strong> All rights
    reserved.
  </footer>


  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- javaScript -->
<script src="{{asset('js/admin.js')}}"></script>
<script>
  $(document).ready(function () {
    $('form').submit(function() {
        $(this).find("button[type='submit']").prop('disabled',true);
    });
    $('.sidebar-menu').tree();
    $('.fileinput').fileinput({
        'showUpload': false,
        'maxFileSize': 1024
    });
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        orientation: 'bottom'
    });
    $('.select2').select2({
        width: 'resolve'
    });

    $('[data-toggle="tooltip"]').tooltip();
  });
</script>

@yield('js')
</body>
</html>
