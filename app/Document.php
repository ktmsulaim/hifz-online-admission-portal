<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Document extends Model
{
    protected $fillable = ['session_id', 'filename', 'title', 'details', 'type'];

    protected $dir = '/files/';
    
    public function session()
    {
        return $this->belongsTo('App\Session');
    }

    public function getFilenameAttribute($val)
    {
        if(!empty($val)){
            return $this->dir.$val;
        }
    }

    public function currentForm()
    {
        return DB::table('current_form')->where('type', '=', 1)->first();
    }

    public static function hasForm()
    {
        if(count(self::where('type', '=', 1)->get()) > 0) {
            return true;
        } else {
            return false;
        }
    }


    public static function hasTicket()
    {
        if(count(self::where('type', '=', 2)->get()) > 0) {
            return true;
        } else {
            return false;
        }
    }
}
