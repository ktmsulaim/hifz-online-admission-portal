@extends('layouts.admin')

@section('page_title')
    <h1>
        Users
    </h1>
@endsection

@section('breadcrumb')
    <li><a href="{{route('users.index')}}">Users</a></li>
    <li class="active"><a>Search</a></li>
@endsection

@section('content')
<div class="box">
    <div class="box-header">
      <h3 class="box-title">Search result for {{'"'.$q.'"'}}</h3>

      <div class="box-tools">
        <form action="{{url('/admin/search/user')}}" method="get">
            <div class="input-group input-group-sm" style="width: 150px;">
                <input type="text" name="q" class="form-control pull-right" value="{{$q}}" placeholder="Search" required>

                <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </form>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body no-padding">
        @if (count($users) > 0)
        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Profile</th>
                    <th>Name</th>
                    <th>Role</th>
                    <th>Status</th>
                    <th>Created</th>
                    <th width="50"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $key => $user)
                    <tr>
                        <td>{{$user->id}}</td>
                        <td><img width="60" src="{{$user->photo ? $user->photo->filename : asset('img/150.png')}}" alt="{{$user->name}}"></td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->is_admin() ? 'Admin' : 'Standard'}}</td>
                        <td>{!! $user->status == 1 ? '<span class="label label-success">Active</span>' : '<span class="label label-danger">Not active</span>' !!}</td>
                        <td>{{date('F d, Y', strtotime($user->created_at))}}</td>
                        <td><a href="{{route('users.show', $user->id)}}"><i class="fa fa-eye fa-fw"></i></a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        @endif
    </div>
    <!-- /.box-body -->

  </div>
@endsection
