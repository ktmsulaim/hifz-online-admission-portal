<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Applicant extends Model
{
    protected $fillable = ['application_id', 'marks', 'rank', 'status'];

    public function application()
    {
        return $this->belongsTo('App\Application');
    }

    public function section()
    {
        return $this->belongsToMany('App\Section')->withPivot('appear', 'appeartime', 'token');
    }

    public function hasSection()
    {
        if ($this->section()->exists()){
            return true;
        }

        return false;
    }

    public function getSection()
    {
        return $this->section()->first();
    }

    public function appearance()
    {
        if($this->hasSection()){
            if($this->getSection()->pivot->appear === 1){
                return true;
            } else {
                return false;
            }
        }
    }

    public function tests()
    {
        return $this->belongsToMany('App\Test')->withPivot('mark', 'remark', 'status');
    }

    public function eligibleForExam()
    {
        if($this->hasSection() && $this->getSection()->pivot->appear === 1) {
            return true;
        }

        return false;
    }

    public static function updateRank()
    {
        $current = Session::current();
        $apts = $current->applicants()->orderBy('marks', 'desc')->get();
        foreach($apts as $key => $val) {
            $val->rank = $key + 1;
            $val->save();
        }
    }

    public function passed(){
        if(count($this->tests) > 0){
            $all = count($this->tests);
            $passed = count($this->tests()->wherePivot('status', '=', 1)->get());

            if($all === $passed) {
                return true;
            } else {
                return false;
            }
        }

        return false;
    }

    public function result()
    {
        return $this->belongsToMany('App\Result');
    }

    public function enrolled(){
        if($this->result()->exists()){
            return true;
        }

        return false;
    }

    public function marks()
    {
        if(count($this->tests) > 0) {
            $max = $this->tests()->sum('max_mark');
            return ($this->marks / $max) * 100;
        } else {
            return 0;
        }
    }
}
