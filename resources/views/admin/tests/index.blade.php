@extends('layouts.admin')

@section('page_title')
    <h1>Examination</h1>
@endsection

@section('breadcrumb')
    <li><a href="#">Examination</a></li>
    <li class="active"><a href="{{route('tests.index')}}">Criteria</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">All tests</h3>
                    <div class="box-tools">
                        <a href="{{route('tests.create')}}" class="btn-box-tool"><i class="fa fa-plus fa-fw"></i> New</a>
                    </div>
                </div>
                <div class="box-body">
                    @if (count($tests) > 0)
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Details</th>
                                    <th>Max.marks</th>
                                    <th>Pass.mark</th>
                                    <th>Status</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                               @foreach ($tests as $key => $test)
                                    <tr>
                                        <td>{{$key + 1}}</td>
                                        <td>{{$test->name}}</td>
                                        <td>{{$test->details}}</td>
                                        <td>{{$test->max_mark}}</td>
                                        <td>{{$test->pass_mark}}</td>
                                        <td>{!! $test->status === 1 ? '<span class="label label-success">ON</span>' : '<span class="label label-danger">OFF</span>' !!}</td>
                                        <td width="50"><a href="{{route('tests.edit', $test->id)}}"><i class="fa fa-edit fa-fw"></i></a></td>
                                    </tr>
                               @endforeach
                            </tbody>
                        </table>
                    @else
                        <p class="text-muted text-center"><i class="fa fa-frown-o fa-f"></i> No tests found!</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
