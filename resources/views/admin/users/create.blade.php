@extends('layouts.admin')

@section('page_title')
    <h1>
        Users
    </h1>
@endsection

@section('breadcrumb')
    <li><a href="{{route('users.index')}}">Users</a></li>
    <li class="active"><a href="{{route('users.create')}}">Create</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8 col-sm-12 col-xs-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">New user</h3>
                </div>
                <div class="box-body">
                    <form action="{{route('users.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('POST')

                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{old('name')}}" required>
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" name="email" value="{{old('email')}}" required>
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" name="password" id="password" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm">Confirm password</label>
                            <input type="password" name="password_confirmation" id="password-confirm" class="form-control" required>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <label for="role">Role</label>
                                <select name="role_id" id="role" class="form-control" required>
                                    <option value="">Select a role</option>
                                    @if (count($roles) > 0)
                                        @foreach ($roles as $role)
                                            <option value="{{$role->id}}">{{$role->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <label for="status">Status</label>
                                <select name="status" id="status" class="form-control" required>
                                    <option value="0">Not active</option>
                                    <option value="1" selected>Active</option>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="form-group">
                            <label for="photo">Profile picture</label>
                            <input type="file" name="photo" id="photo" class="fileinput">
                        </div>

                        <div class="text-right">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-plus fa-fw"></i> Create</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
            @if (count($errors) > 0)
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Errors</h3>
                    </div>
                    <div class="box-body">
                        <ul class="text-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif
        </div>
        <div class="clearfix"></div>
    </div>
@endsection
