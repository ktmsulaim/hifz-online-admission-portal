<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sessions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('year');
            $table->string('from');
            $table->string('to');
            $table->string('birth_min');
            $table->string('birth_max');
            $table->integer('max_count')->unsigned(); // Max number of enrolled students
            $table->boolean('is_current')->default(0);
            $table->boolean('status')->default(0);
            // (Datetime) Interview date from, to
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sessions');
    }
}
