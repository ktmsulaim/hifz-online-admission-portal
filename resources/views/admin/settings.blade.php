@extends('layouts.admin')

@section('page_title')
    <h1>
        Settings
    </h1>
@endsection

@section('breadcrumb')
    <li class="active"><a href="{{route('settings')}}">Settings</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab">General</a></li>
                  <li><a href="#tab_2" data-toggle="tab">Session</a></li>
                  <li><a href="#tab_3" data-toggle="tab">Application</a></li>
                </ul>
                <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
                    <b>About institution</b>
                    <div class="form-wrapper">
                        <form action="{{route('updateAbout')}}" method="post">
                            @csrf
                            <table class="table">
                                <tr>
                                    <td width="100">Name</td>
                                    <td><input type="text" class="form-control" name="name" value="{{old('name', $about ? $about->name : '')}}" required></td>
                                    <td width="100">Contact no</td>
                                    <td><input type="number" placeholder="Contact number 1" name="contact" id="contact" min="0" class="form-control" value="{{old('contact', $about ? $about->contact : '')}}" required>
                                        <input type="number" placeholder="Contact number 2" name="contact2" id="contact2" min="0" class="form-control" value="{{old('contact2', $about ? $about->contact2 : '')}}" required></td>
                                </tr>
                                <tr>
                                    <td width="100">Address</td>
                                    <td><textarea name="address" id="address" cols="30" rows="3" style="resize:none" class="form-control" required>{{old('address', $about ? $about->address : '')}}</textarea></td>
                                    <td width="100">Email</td>
                                    <td><input type="email" name="email" id="email" class="form-control" value="{{old('email', $about ? $about->email : '')}}" required></td>
                                </tr>
                                <tr>
                                    <td width="100">Description</td>
                                    <td><textarea name="description" id="desc" cols="30" rows="5" style="resize:none" class="form-control" required>{{old('description', $about ? $about->description : '')}}</textarea></td>
                                    <td width="100">Website</td>
                                    <td><input type="url" name="website" id="website" class="form-control" value="{{old('website', $about ? $about->website : '')}}" required></td>
                                </tr>
                            </table>
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                    <p>Give users instructions that's why they can understand the admission procedures. <a href="{{route('instructions')}}">Set up</a></p>
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="tab_2">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            @if ($session)
                            <div class="text-right">
                                <a class="btn btn-link" href="{{route('session.index')}}">Change</a>
                            </div>
                            <table class="table">
                                <tr>
                                    <th>Current</th>
                                    <td>{{$session->name}} <a href="{{route('session.edit', $session->id)}}"><i title="Edit" class="fa fa-edit fa-fw"></i></a></td>
                                    <td>Name of current session.</td>
                                </tr>
                                <tr>
                                    <th>Year</th>
                                    <td>{{$session->year}}</td>
                                    <td>The year of session, can be used in the application form.</td>
                                </tr>
                                <tr>
                                    <th>From</th>
                                    <td>{{$session->from}}</td>
                                    <td>Date of online registration starts</td>
                                </tr>
                                <tr>
                                    <th>To</th>
                                    <td>{{$session->to}}</td>
                                    <td>Deadline of registration, days left is calculated based on this date.</td>
                                </tr>
                                <tr>
                                    <th>Max. Count</th>
                                    <td>{{$session->max_count}}</td>
                                    <td>Maximum number of students can be enrolled in this session.</td>
                                </tr>
                                <tr>
                                    <th>DOB min</th>
                                    <td>{{$session->birth_min}}</td>
                                    <td>Minimum date of birth date of applicant</td>
                                </tr>
                                <tr>
                                    <th>DOB max</th>
                                    <td>{{$session->birth_max}}</td>
                                    <td>Maximum date of birth date of applicant</td>
                                </tr>
                                <tr>
                                    <th>Status</th>
                                    <td>{!! $session->status == 1 ? '<span class="label label-success">ON</span>' : '<span class="label label-danger">OFF</span>' !!}</td>
                                    <td>Status of admission, whether it is open or closed.</td>
                                </tr>
                            </table>

                            @else
                                <p class="text-muted text-center"><i class="fa fa-frown-o fa-fw"></i> No current session found, set it <a href="{{route('session.index')}}">now</a></p>
                            @endif
                        </div>
                    </div>
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="tab_3">
                      <div>
                          <a href="{{route('documents.index')}}" class="btn btn-link"><i class="fa fa-files-o fa-fw"></i> Documents</a>
                      </div>
                    <table class="table table-bordered">
                        <tr>
                            <th width="150">Form</th>
                            <td>
                                @if ($form)
                                    <iframe src="{{asset('/ViewerJS/#..'.$form->filename)}}" frameborder="0" width="500" height="300" allowfullscreen webkitallowfullscreen></iframe>
                                    <div class="form-details">
                                        <b>{{$form->title}} <a data-toggle="tooltip" data-original-title="Edit" data-placement="right" href="{{route('documents.edit', $form->id)}}"><i class="fa fa-edit fa-fw"></i></a></b>
                                        <p>{{$form->details}}</p>
                                    </div>
                                @else
                                    <p class="text-muted">
                                        <i class="fa fa-frown-o fa-fw"></i> No form file has been set. Upload it <a href="{{route('documents.create')}}">now</a>!
                                    </p>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Hall ticket</th>
                            <td>
                                @if ($hallTicket)
                                    <iframe src="{{asset('/ViewerJS/#..'.$hallTicket->filename)}}" frameborder="0" width="500" height="300" allowfullscreen webkitallowfullscreen></iframe>
                                    <div class="form-details">
                                        <b>{{$hallTicket->title}} <a data-toggle="tooltip" data-original-title="Edit" data-placement="right" href="{{route('documents.edit', $hallTicket->id)}}"><i class="fa fa-edit fa-fw"></i></a></b>
                                        <p>{{$hallTicket->details}}</p>
                                    </div>
                                @else
                                    <p class="text-muted">
                                        <i class="fa fa-frown-o fa-fw"></i> No hall ticket file has been set. Upload it <a href="{{route('documents.create')}}">now</a>!
                                    </p>
                                @endif
                            </td>
                        </tr>
                    </table>
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div>
        </div>
    </div>
@endsection
