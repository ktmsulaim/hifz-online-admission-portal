<?php

namespace App\Http\Controllers;

use App\About;
use App\Session;
use App\User;
use App\Visitor;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;

class AdminHomeController extends Controller
{
    public function index()
    {
        $users = count(User::all());
        $visitors = count(Visitor::all());
        $current = Session::current();
        return view('admin.index', compact('users', 'current', 'visitors'));
    }

    public function settings()
    {
        if(Gate::allows('settings_access')) {
            $session = Session::current();
            $form = $session->documents->where('type', '=', 1)->first();
            $hallTicket = $session->documents->where('type', '=', 2)->first();
            $about = About::first();
            return view('admin.settings', compact('session', 'form', 'hallTicket', 'about'));
        } else {
            return back();
        }
    }

    public function updateAbout(Request $request) {
        if(Gate::allows('super_access')) {
            $about = About::first();
            if($about === null) {
                About::create($request->all());
            } else {
                $about->update($request->all());
            }

            return back()->with('success', 'The info has been updated!');
        } else {
            return back();
        }
    }

    public function instructions()
    {
        if(Gate::allows('super_access')) {
            $current = Session::current();
            $inst = $current->instruction;
            return view('admin.instructions', compact('current', 'inst'));
        } else {
            return back();
        }
    }

    public function updateInst(Request $request) {
        if(Gate::allows('super_access')) {
            $current = Session::current();
            if($current->instruction()->exists()) {
                $inst = $current->instruction;
                $inst->update($request->all());
            } else {
                $inst = $current->instruction()->create(['data' => $request->data]);
            }
            return back()->with('success', 'The instruction has been updated!');
        } else {
            return back();
        }
    }
}
