@extends('layouts.admin')

@section('page_title')
    <h1>Examination</h1>
@endsection

@section('breadcrumb')
    <li><a href="#">Examination</a></li>
    <li class="active"><a href="{{route('markentry')}}">Mark entry</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Select applicant</h3>
                </div>
                <div class="box-body">
                    <form action="{{route('findapt')}}" method="get">
                        @csrf
                        <div class="form-group">
                            <label for="id">Ref.no</label>
                            <select name="id" id="id" class="form-control select2" required>
                                <option value="">Select an applicant</option>
                                @if (count($applicants) > 0)
                                    @foreach ($applicants as $apt)
                                        @if ($apt->eligibleForExam())
                                            <option value="{{$apt->id}}">{{$apt->application->refno().' - '.$apt->application->name}}</option>
                                        @endif
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="text-right">
                            <button class="btn btn-primary" type="submit"><i class="fa fa-send fa-fw"></i> Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Instructions</h3>
                </div>
                <div class="box-body">
                    <p>The details of tests are given below:</p>
                    @if (count($current->tests) > 0)
                    <ol>
                        @foreach ($current->tests as $test)
                            <li><strong>{{$test->name}}: </strong> {{$test->details}}</li>
                        @endforeach
                    </ol>
                    @else
                        <p class="text-muted"><i class="fa fa-frown-o fa-fw"></i> No test was created, contact the admin for more information.</p>
                    @endif
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
@endsection
