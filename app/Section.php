<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Section extends Model
{
    protected $fillable = ['session_id', 'name', 'start', 'end', 'details', 'count'];

    public function applicants()
    {
        return $this->belongsToMany('App\Applicant')->withPivot('appear', 'appeartime', 'token');
    }

    public static function resetToken($id){
        DB::statement("SET @count = 0;");
        DB::statement("UPDATE applicant_section SET applicant_section.token = @count:= @count + 1 WHERE applicant_section.section_id = {$id}");
    }

    public function checkAvail(){
        if(count($this->applicants) === $this->count){
            return false;
        }

        return true;
    }

    public function avail()
    {
        if($this->checkAvail()) {
            return $this->count - count($this->applicants);
        }

        return 0;
    }
}
