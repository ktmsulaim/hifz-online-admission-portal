<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('session_id')->unsigned()->index();
            $table->integer('user_id')->unsigned()->index();
            $table->integer('ref_no');
            $table->string('name');
            // $table->string('name_of_home');
            // $table->string('name_of_father');
            // $table->string('job_of_father');
            $table->string('dob');
            $table->string('dob_details');
            // $table->string('dob_hijri');
            // $table->string('reason_not_father')->nullable();
            // $table->string('name_of_guard')->nullable();
            // $table->string('relation_of_guard')->nullable();
            // $table->string('job_of_guard')->nullable();
            // $table->string('name_of_mahall');
            $table->string('district');
            $table->string('province');
            $table->string('village');
            $table->string('mobile');
            $table->string('landphone_std');
            $table->string('landphone');
            $table->string('name_of_madrassa');
            $table->integer('madrassa_completed_class');
            $table->string('madrassa_number');
            // $table->string('name_of_range');
            // $table->string('date_of_exam');
            // $table->string('name_of_school');
            // $table->integer('school_completed_class');
            // $table->string('identification_mark1');
            // $table->string('identification_mark2');
            // $table->text('postal_address');
            // $table->string('post_office');
            // $table->string('post_district');
            // $table->integer('pincode');
            $table->boolean('status')->default(0);
            $table->timestamps();

            $table->foreign('session_id')->references('id')->on('sessions')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}
