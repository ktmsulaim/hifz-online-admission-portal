@extends('layouts.client')

@section('page_title')
    <h1>
        Profile
    </h1>
@endsection


@section('breadcrumb')
    <li class="active"><a href="{{url('/client/profile')}}">Profile</a></li>
@endsection


@section('content')
    <div class="row">
        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
            <div class="box box-widget widget-user-2">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-aqua-active">
                  <div class="widget-user-image">
                    <img class="img-circle" src="{{Auth::user()->photo ? asset(Auth::user()->photo->filename) : asset('img/150.png')}}" alt="User image">
                  </div>
                  <!-- /.widget-user-image -->
                  <h3 class="widget-user-username">{{Auth::user()->name}}</h3>
                  <h5 class="widget-user-desc">{{Auth::user()->email}}</h5>
                </div>
                <div class="box-body no-padding">
                  <ul class="nav nav-stacked">
                    <li><a href="#">Role <span class="pull-right badge bg-blue">{{Auth::user()->hasAnyRole() ? ucfirst(Auth::user()->role()->first()->name) : 'No role'}}</span></a></li>
                    <li><a href="#">Status {!! Auth::user()->status === 1 ? '<span class="pull-right badge bg-green">Active</span>' : '<span class="pull-right badge bg-red">Not active</span>' !!}</a></li>
                  </ul>
                </div>
                <div class="box-footer text-right">
                    <a href="{{url('/client/profile/changepass')}}"><i class="fa fa-lock fa-fw"></i> Change password</a>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
@endsection