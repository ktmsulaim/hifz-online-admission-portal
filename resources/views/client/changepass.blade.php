@extends('layouts.client')

@section('page_title')
    <h1>
        Profile
    </h1>
@endsection


@section('breadcrumb')
    <li><a href="{{url('/client/profile')}}">Profile</a></li>
    <li class="active"><a href="{{url('/client/profile/changepass')}}">Change password</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8 col-lg-8 col-sm-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Change your password</h3>
                </div>
                <div class="box-body">
                    <form action="{{route('changePassword')}}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="current">Current password</label>
                            <input type="password" name="current" id="password" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="password">New password</label>
                            <input type="password" name="password" id="password" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="password_confirmation">Current password</label>
                            <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" required>
                        </div>
    
                        <div class="text-right">
                            <button class="btn btn-primary" type="submit">Change</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
            @if (count($errors) > 0)

            <div class="box box-danger">
                <div class="box-header">
                    <h3 class="box-title">Errors</h3>
                </div>
                <div class="box-body">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
                
            @endif
        </div>
        <div class="clearfix"></div>
    </div>
@endsection