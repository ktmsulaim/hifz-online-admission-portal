<?php

namespace App\Http\Controllers;

use App\Applicant;
use App\Session;
use App\Test;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Gate::allows('super_access')){
            $current = Session::current();
            $tests = $current->tests;
            return view('admin.tests.index', compact('current', 'tests'));
        } else {
            return back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Gate::allows('super_access')){
            $current = Session::current();
            return view('admin.tests.create', compact('current'));
        } else {
            return back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Gate::allows('super_access')){
            $request->validate([
                'name' => 'required',
                'details' => 'required',
                'max_mark' => 'required|integer',
                'pass_mark' => 'required|integer'
            ]);

            $test = Test::create($request->all());
            foreach(Applicant::all() as $apt){
                $test->applicants()->attach($apt->id);
            }
            return redirect()->route('tests.index');
        } else {
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function show(Test $test)
    {
        if(Gate::allows('super_access')){

        } else {
            return back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Gate::allows('super_access')){
            $test = Test::findOrFail($id);
            $current = Session::current();

            return view('admin.tests.edit', compact('test', 'current'));
        } else {
            return back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Gate::allows('super_access')){
            $request->validate([
                'name' => 'required',
                'details' => 'required',
                'max_mark' => 'required|integer',
                'pass_mark' => 'required|integer'
            ]);

            $test = Test::findOrFail($id);
            $test->update($request->all());
            return redirect()->route('tests.index');
        } else {
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Gate::allows('super_access')){
            $test = Test::findOrFail($id);
            $test->delete();
            return redirect()->route('tests.index');
        } else {
            return back();
        }
    }
}
