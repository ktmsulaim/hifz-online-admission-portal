@extends('layouts.admin')

@section('page_title')
    <h1>Verification items</h1>
@endsection

@section('breadcrumb')
    <li class="active"><a href="{{route('vitems.index')}}">Verification items</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">All items</h3>
                </div>
                <div class="box-body">
                    @if (count($vitems) > 0)
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Type</th>
                                    <th>Created on</th>
                                    <th width="50"></th>
                                    <th width="50"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($vitems as $key => $vitem)
                                <tr>
                                    <td>{{$key + 1}}</td>
                                    <td>{{$vitem->name}}</td>
                                    <td>{{$vitem->type->name}}</td>
                                    <td>{{date('F d, Y h:i A', strtotime($vitem->created_at))}}</td>
                                    <td><a class="btn btn-link btn-sm" href="{{route('vitems.edit', $vitem->id)}}"><i class="fa fa-edit fa-fw"></i></a></td>
                                    <td>
                                        <form action="{{route('vitems.destroy', $vitem->id)}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-link btn-sm" type="submit"><i class="fa fa-trash fa-fw"></i></button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                    <p class="text-center text-muted"><i class="fa fa-frown-o fa-fw"></i> No items found!</p>
                    @endif
                </div>
                <div class="box-footer text-right">
                    {{$vitems->links()}}
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Create an item</h3>
                </div>
                <div class="box-body">
                    <form action="{{route('vitems.store')}}" method="post">
                        @csrf
                        @method('POST')

                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" name="name" id="name" class="form-control" value="{{old('name')}}" required>
                        </div>

                        <div class="form-group">
                            <label for="type_id">Type</label>
                            <select name="type_id" id="type_id" class="form-control" required>
                                <option value="">Select a type</option>
                                @if (count($types) > 0)
                                    @foreach ($types as $type)
                                        <option value="{{$type->id}}">{{$type->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>

                        <div class="text-right">
                            <button class="btn btn-primary" type="submit"><i class="fa fa-plus fa-fw"></i> Create</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
@endsection
