<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Darul Hasanath Hifz al Quran College | Result</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('css/home.css')}}">
    <link rel="shortcut icon" href="{{asset('img/logo.png')}}" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,900;1,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;400;600;900&display=swap" rel="stylesheet">
</head>
<body>

    <header id="result">
        <nav>
            <div class="brand">
                <a href="{{route('welcome')}}" class="text-white">DHHQC</a>
            </div>
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a id="home" class="button" href="{{ url('/home') }}">Home</a>
                    @else
                        <a id="login" class="button" href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a id="register" class="button" href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif
        </nav>
    </header>
    <main>
        <section class="section lite">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="section-head">
                            <h2>Enrolled applicants</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 m-auto">
                        @if (count($result->applicants) > 0)
                            <table class="table table-hover table-responsive-sm table-bordered">
                                <thead>
                                    <tr>
                                        <th width="">Rank</th>
                                        <th width="">Ref.No</th>
                                        <th>Name</th>
                                        <th width="">Marks</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($result->applicants as $apt)
                                    <tr>
                                        <td>{{$apt->rank}}</td>
                                        <td>{{$apt->application->refno()}}</td>
                                        <td>{{$apt->application->name}}</td>
                                        <td>{{$apt->marks()}}%</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                        <p class="text-muted text-center">Sorry! No applicants were enrolled!</p>
                        @endif
                    </div>
                </div>
            </div>
        </section>
    </main>

        <footer>
            <span>{{date('Y')}} &copy; Darul Hasanath Islamic Complex. All rights reserved. Developed by <a href="http://sacreationsknr.info">SA Creations</a></span>
        </footer>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    </body>
</html>
