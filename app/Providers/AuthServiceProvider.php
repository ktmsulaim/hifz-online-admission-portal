<?php

namespace App\Providers;

use App\Application;
use App\Policies\ApplicationPolicy;
use App\Policies\UserPolicy;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // User::class => UserPolicy::class,
        // Application::class => ApplicationPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        $this->registerUserPolicies();

        //
    }

    public function registerUserPolicies()
    {
        Gate::define('super_access', function($user){
            if($user->hasRole('admin')){
                return true;
            }
        });

        Gate::define('users_access', function($user){
            if ($user->hasRole('admin')) {
                return true;
            }
        });

        Gate::define('application_access', function($user) {
            $allowed = ['admin', 'helpdesk'];

            foreach($allowed as $role) {
                if($user->hasRole($role)) {
                    return true;
                }
            }
        });

        Gate::define('verification_access', function($user) {
            if ($user->hasRole('admin')) {
                return true;
            }
        });

        Gate::define('applicants_access', function($user){
            if ($user->hasRole('admin')) {
                return true;
            }
        });

        Gate::define('examination_access', function($user) {
            $allowed = ['admin', 'examiner'];

            foreach($allowed as $role) {
                if($user->hasRole($role)) {
                    return true;
                }
            }
        });

        Gate::define('settings_access', function($user){
            if ($user->hasRole('admin')) {
                return true;
            }
        });

    }
}
