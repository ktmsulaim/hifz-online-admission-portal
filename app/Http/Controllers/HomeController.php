<?php

namespace App\Http\Controllers;

use App\Session;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::user()->hasAnyRole()){
            if(Auth::user()->hasRole('standard') === false){
                return redirect()->route('admin');
            }else{
                return redirect()->route('client');
            }
        } else {
             return redirect()->route('logout');
        }
    }

    public function calcage(Request $request) {
         // convert date to this format
        $dob = date('d-m-Y', strtotime(str_replace('/', '-', $request->age)));
        $target = date('Y-m-d', strtotime(str_replace('/', '-', $request->target)));

        $date1 = new DateTime($dob);
        $date2 = new DateTime($target);

        $interval = $date2->diff($date1);
        echo $interval->y . " years, " . $interval->m." months, ".$interval->d." days";
        // echo $dob;
    }
}
