<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = ['name'];

    public function user()
    {
        return $this->belongsToMany('App\User');
    }

    public function permission()
    {
        return $this->belongsToMany('App\Permission');
    }

    public function hasPermssion()
    {

    }
}
