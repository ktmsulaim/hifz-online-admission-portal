@extends('layouts.admin')

@section('page_title')
    <h1>
        Sessions
    </h1>
@endsection

@section('breadcrumb')
    <li><a href="{{route('session.index')}}">Sessions</a></li>
    <li class="active"><a href="{{route('session.edit', $session->id)}}">Edit</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8 col-sm-6 col-xs-8">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Edit session</h3>
                </div>
                <div class="box-body">
                    <form action="{{route('session.update', $session->id)}}" method="post">
                        @csrf
                        @method('PATCH')

                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" name="name" class="form-control" id="name" value="{{old('name', $session->name)}}" required>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <label for="from">From</label>
                                <input type="text" name="from" class="datepicker form-control" value="{{old('from', $session->from)}}" required>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <label for="to">To</label>
                                <input type="text" name="to" class="datepicker form-control" value="{{old('to', $session->to)}}" required>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <label for="birth_min">DOB min</label>
                                <input type="text" name="birth_min" class="datepicker form-control" value="{{old('birth_min', $session->birth_min)}}" placeholder="Minimum date of birth date" required>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <label for="birth_max">DOB max</label>
                                <input type="text" name="birth_max" class="datepicker form-control" value="{{old('birth_max', $session->birth_max)}}" placeholder="Maximum date of birth date" required>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <label for="year">Year</label>
                                <input type="number" min="2000" name="year" id="year" class="form-control" value="{{old('year', $session->year)}}" required>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <label for="max_count">Max.Count</label>
                                <input type="number" name="max_count" min="0" id="max_count" class="form-control" value="{{old('max_count', $session->max_count)}}" required>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <label for="is_current">Is current</label>
                                <select name="is_current" id="is_current" class="form-control" required>
                                    <option value="0" {{$session->is_current == 0 ? 'selected' : ''}}>No</option>
                                    <option value="1" {{$session->is_current == 1 ? 'selected' : ''}}>Yes</option>
                                </select>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <label for="status">Status</label>
                                <select name="status" id="status" class="form-control" required>
                                    <option value="0" {{$session->status == 0 ? 'selected' : ''}}>Closed</option>
                                    <option value="1" {{$session->status == 1 ? 'selected' : ''}}>Open</option>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="text-right">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-send fa-fw"></i> Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">Delete</h3>
                </div>
                <div class="box-body">
                    <p>You are going to delete this session. This delete all data related to this session.</p>
                    <form action="{{route('session.destroy', $session->id)}}" method="post">
                        @csrf
                        @method('DELETE')
                        <div class="text-right">
                            <button type="submit" class="btn btn-danger"><i class="fa fa-trash fa-fw"></i> Delete</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
@endsection
