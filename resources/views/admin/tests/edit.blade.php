@extends('layouts.admin')

@section('page_title')
    <h1>Examination</h1>
@endsection

@section('breadcrumb')
    <li><a href="#">Examination</a></li>
    <li><a href="{{route('tests.index')}}">Criteria</a></li>
    <li class="active"><a>Edit</a></li>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Edit test</h3>
            </div>
            <div class="box-body">
                <form action="{{route('tests.update', $test->id)}}" method="post">
                    @csrf
                    @method('PATCH')
                    <input type="hidden" name="session_id" value="{{$current->id}}">

                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" id="name" value="{{old('name', $test->name)}}" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label for="details">In detail</label>
                        <textarea type="text" name="details" id="details" class="form-control" style="resize:none" rows="4" required>{{old('details', $test->details)}}</textarea>
                    </div>

                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                            <label for="max_mark">Maximum marks</label>
                            <input type="number" min="0" name="max_mark" id="max_mark" value="{{old('max_mark', $test->max_mark)}}" class="form-control" required>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                            <label for="pass_mark">Pass mark</label>
                            <input type="number" min="0" name="pass_mark" id="pass_mark" value="{{old('pass_mark', $test->pass_mark)}}" class="form-control" required>
                        </div>
                    </div>

                    <div class="text-right">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-send fa-fw"></i> Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Delete</h3>
            </div>
            <div class="box-body">
                <p>You are going to delete this test. The data can't be recovered once deleted. Continue?</p>
                <form action="{{route('tests.destroy', $test->id)}}" method="post">
                    @csrf
                    @method('DELETE')
                    
                    <div class="text-right">
                        <button type="submit" class="btn btn-danger"><i class="fa fa-trash-o fa-fw"></i> Delete</button>
                    </div>
                </form>
            </div>
        </div>
        @if ($errors->any())
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">Errors</h3>
                </div>
                <div class="box-body">
                    <ul>
                        @foreach ($errors as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
    </div>
</div>
@endsection

