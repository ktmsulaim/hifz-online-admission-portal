<?php

namespace App\Http\Controllers;

use App\Session;
use App\Type;
use App\VerificationItem;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class VerificationItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Gate::allows('verification_access')){
            $vitems = VerificationItem::paginate(10);
            $types = Type::all();
            return view('admin.validation.items.index', compact('vitems', 'types'));
        } else {
            return back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Gate::allows('verification_access')){
            $vitems = VerificationItem::paginate(10);
            $types = Type::all();
            return view('admin.validation.items.index', compact('vitems', 'types'));
        } else {
            return back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Gate::allows('verification_access')){
            $vitem = VerificationItem::create($request->all());

            // assign to all applications
            $current = Session::current();
            $apps = $current->applications;

            if(count($apps)) {
                foreach($apps as $app) {
                    $app->verificationItems()->attach($vitem->id, ['status' => 0, 'approvedby' => Auth::id(), 'created_at' => Carbon::now()]);
                }
            }
            return back();
        } else {
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\VerificationItem  $verificationItem
     * @return \Illuminate\Http\Response
     */
    public function show(VerificationItem $verificationItem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\VerificationItem  $verificationItem
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Gate::allows('verification_access')) {
            $vitem = VerificationItem::findOrFail($id);
            $types = Type::all();
            return view('admin.validation.items.edit', compact('vitem', 'types'));
        } else {
            return back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\VerificationItem  $verificationItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Gate::allows('verification_access')) {
            $vitem = VerificationItem::findOrFail($id);
            $vitem->update($request->all());
            return redirect()->route('vitems.index');
        } else {
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\VerificationItem  $verificationItem
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Gate::allows('verification_access')) {
            $vitem = VerificationItem::findOrFail($id);
            $vitem->delete();
            return back();
        } else {
            return back();
        }
    }
}
