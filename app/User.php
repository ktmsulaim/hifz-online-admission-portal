<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'is_admin', 'name', 'email', 'password', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function is_active()
    {
        if($this->status === 1){
            return true;
        }
        return false;
    }

    public function hasAnyRole()
    {
        if($this->role()->exists()){
            return true;
        }

        return false;
    }

    public function hasRole($role)
    {
        $roles = $this->role()->pluck('name')->toArray();

        if($roles){
            if (in_array($role, $roles)) {
                return true;
            }
        }

        return false;
    }

    public function is_admin()
    {
        if($this->hasAnyRole()){
            if($this->hasRole('admin')){
                return true;
            }
        }

        return false;
    }


    public function is_client()
    {
        if($this->hasAnyRole()){
            if($this->hasRole('standard')){
                return true;
            }
        }
        return false;
    }

    public function photo()
    {
        return $this->morphOne('App\Photo', 'photoable');
    }

    public function role()
    {
        return $this->belongsToMany('App\Role', 'role_user');
    }

    public function applications()
    {
        return $this->hasMany('App\Application');
    }

    public function applicants(){
        return $this->hasManyThrough('App\Applicant', 'App\Application');
    }
}
