<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()){
            if(Auth::user()->hasAnyRole() && Auth::user()->hasRole('standard') === false) {
                if(Auth::user()->is_active()){
                    return $next($request);
                }else{
                    return redirect()->route('logout');
                }
            }else{
                return redirect()->route('logout');
            }
        }else{
            return redirect()->route('logout');
        }
    }
}
