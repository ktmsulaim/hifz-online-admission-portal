@extends('layouts.admin')

@section('page_title')
    <h1>Settings</h1>
@endsection

@section('breadcrumb')
    <li><a href="{{route('settings')}}">Settings</a></li>
    <li class="active"><a href="{{route('instructions')}}">Instructions</a></li>
@endsection

@section('content')
    @if (session('success'))
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-check"></i> Success!</h4>
                    {{session('success')}}
                </div>
            </div>
        </div>
    @endif
    <div class="row">   
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Instructions</h3>
                </div>
                <div class="box-body">
                    <form action="{{Route('updateInst')}}" method="post">
                        @csrf
                        <div class="form-group">
                            <textarea style="resize:none" name="data" id="data" cols="30" rows="10" class="form-control" required>{{old('data', $inst ? $inst->data : '')}}</textarea>
                        </div>
                        <div class="text-right">
                            <button class="btn btn-primary" type="submit"><i class="fa fa-send fa-fw"></i> Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{asset('js/ckeditor/ckeditor.js')}}"></script>
    <script>
        $(function(){
            CKEDITOR.replace('data')
        });
    </script>
@endsection