@extends('layouts.client')

@section('page_title')
    <h1>
        Home
    </h1>
@endsection

@section('content')
    @if ($current->hasResult() && $current->results->first()->status === 1)
        @if(count(Auth::user()->applicants) > 0)
            @foreach(Auth::user()->applicants as $apt)
                @if($apt->status === 1)
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-check"></i> Congrats!</h4>
                            The applicant {{$apt->application->name}} has been enrolled! <a href="{{route('application.show', $apt->application->id)}}">View</a>
                          </div>
                    </div>
                </div>
                @endif
            @endforeach
        @endif
    @endif
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Instructions</h3>
                </div>
                <div class="box-body">
                    @if ($inst)
                        {!! $inst->data !!}

                    @else
                        <p class="text-muted">Please stay tuned! <i class="fa fa-smile-o fa-fw"></i></p>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection