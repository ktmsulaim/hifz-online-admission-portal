@extends('layouts.admin')

@section('page_title')
    <h1>
        Users
    </h1>
@endsection

@section('breadcrumb')
    <li><a href="{{route('users.index')}}">Users</a></li>
    <li class="active"><a href="{{route('users.show', $user->id)}}">View</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="box box-primary">
                <div class="box-body box-profile">
                  <img class="profile-user-img img-responsive img-circle" style="height:100px" src="{{$user->photo ? $user->photo->filename : asset('img/150.png')}}" alt="{{$user->name}}">
    
                  <h3 class="profile-username text-center">{{$user->name}}</h3>
    
                  <p class="text-muted text-center">{{$user->email}}</p>
    
                  <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                      <b>Role</b> <a class="pull-right">{{$user->hasAnyRole() ? $user->role()->first()->name : 'No role'}}</a>
                    </li>
                    <li class="list-group-item">
                      <b>Status</b> <a class="pull-right">{!! $user->status == 1 ? '<span class="label label-success">Active</span>' : '<span class="label label-danger">Not active</span>' !!}</a>
                    </li>
                    <li class="list-group-item">
                      <b>Created on</b> <a class="pull-right">{{date('F d, Y g:i A', strtotime($user->created_at))}}</a>
                    </li>
                  </ul>
    
                  <a href="{{route('users.edit', $user->id)}}" class="btn btn-primary btn-block"><b>Edit</b></a>
                </div>
                <!-- /.box-body -->
              </div>
        </div>
        <div class="col-md-8 col-sm-6 col-xs-12"></div>
        <div class="clearfix"></div>
    </div>
@endsection