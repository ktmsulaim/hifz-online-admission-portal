<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ApplicationUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'session_id' => 'required|integer',
            'user_id' => 'required|integer',
            'photo' => 'image|mimes:jpg,jpeg,png,gif|max:512',
            'ref_no' => 'required',
            'name' => 'required|string|max:40',
            'dob' => 'required|string|max:40',
            'district' => 'required|string|max:40',
            'province' => 'required|string|max:40',
            'village' => 'required|string|max:40',
            'mobile' => 'required|numeric|digits_between:1,40',
            'landphone_std' => 'required|numeric|digits_between:1,4',
            'landphone' => 'required|numeric|digits_between:1,7',
            'name_of_madrassa' => 'required|string|max:40',
            'madrassa_completed_class' => 'required|integer|digits_between:1,2',
            'madrassa_number' => 'required|integer|digits_between:1,10'
        ];
    }
}
