@extends('layouts.admin')

@section('page_title')
    <h1>
        Users
    </h1>
@endsection

@section('breadcrumb')
    <li><a href="{{route('users.index')}}">Users</a></li>
    <li><a>Edit</a></li>
    <li class="active"><a href="{{route('users.edit', $user->id)}}">{{$user->name}}</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8 col-sm-12 col-xs-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit user</h3>
                </div>
                <div class="box-body">
                    <form action="{{route('users.update', $user->id)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')

                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{old('name', $user->name)}}" required>
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" name="email" value="{{old('email', $user->email)}}" required>
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" name="password" id="password" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="password-confirm">Confirm password</label>
                            <input type="password" name="password_confirmation" id="password-confirm" class="form-control">
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <label for="role">Role</label>
                                <select name="role_id" id="role" class="form-control" required>
                                    <option value="">Select a role</option>
                                    @if (count($roles) > 0)
                                        @foreach ($roles as $role)
                                            <option value="{{$role->id}}" {{$user->hasAnyRole() && $user->role()->first()->id == $role->id ? 'selected' : ''}}>{{$role->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <label for="status">Status</label>
                                <select name="status" id="status" class="form-control" required>
                                    <option value="0" {{$user->status == 0 ? 'selected' : ''}}>Not active</option>
                                    <option value="1" {{$user->status == 1 ? 'selected' : ''}}>Active</option>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="form-group">
                            <label for="photo">Profile picture</label>
                            <input type="file" name="photo" id="photo">
                        </div>

                        <div class="text-right">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-send fa-fw"></i> Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="box box-danger">
                <div class="box-header">
                    <h3 class="box-title text-danger">Delete</h3>
                </div>
                <div class="box-body">
                    <p>You are going to delete this user. Be in mind you can't recover data once you have deleted.</p>
                    <div class="text-right">
                        <form action="{{route('users.destroy', $user->id)}}" method="post">
                            @csrf
                            @method('DELETE')

                            <button type="submit" class="btn btn-danger"><i class="fa fa-trash fa-fw"></i> Delete</button>
                        </form>
                    </div>
                </div>
            </div>
            @if (count($errors) > 0)
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Errors</h3>
                    </div>
                    <div class="box-body">
                        <ul class="text-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif
        </div>
        <div class="clearfix"></div>
    </div>
@endsection

@section('js')
    <script>
        $(function(){
            $('#photo').fileinput({
                showUpload: false,
               @if($user->photo)
                    initialPreview: '<img width="100%" src={{asset($user->photo->filename)}}>'
               @endif
            });
        });
    </script>
@endsection
