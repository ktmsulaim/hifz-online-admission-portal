<?php

namespace App\Http\Controllers;

use App\Document;
use App\Http\Requests\DocumentEditRequest;
use App\Http\Requests\DocumentRequest;
use App\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Gate::allows('settings_access')) {
            $session = Session::current();
            $docs = $session->documents()->paginate(10);
            return view('admin.documents.index', compact('session', 'docs'));
        } else {
            return back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Gate::allows('settings_access')) {
            $session = Session::current();
            return view('admin.documents.create', compact('session'));
        } else {
            return back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DocumentRequest $request)
    {
        if(Gate::allows('settings_access')) {
            $input = $request->all();
            // dd((int)$input['type']);
            if($request->hasFile('filename') && $request->file('filename')->isValid()){
                $file = $request->file('filename');
                $filename = time().'.'.$file->getClientOriginalExtension();
                $file->move('files', $filename);
                $input['filename'] = $filename;
            }
            
            $input['type'] = (int)$request->type;

            Document::create($input);
            return redirect()->route('documents.index')->with('success', 'The document has been created!');
        } else {
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function show(Document $document)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Gate::allows('settings_access')) {
            $session = Session::current();
            $doc = $session->documents->find($id);
            return view('admin.documents.edit', compact('doc', 'session'));
        } else {
            return back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function update(DocumentEditRequest $request, $id)
    {
        if(Gate::allows('settings_access')) {
            $input = $request->all();
            
            if($request->hasFile('filename') && $request->file('filename')->isValid()){
                $file = $request->file('filename');
                $filename = time().'.'.$file->getClientOriginalExtension();
                $file->move('files', $filename);
                $input['filename'] = $filename;
            }
            
            $input['type'] = (int)$request->type;

            $doc = Document::findOrFail($id);
            $doc->update($input);
            return redirect()->route('documents.index')->with('success', 'The document has been updated!');
        } else {
            return back();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Gate::allows('settings_access')) {
            $doc = Document::findOrFail($id);

            if(file_exists(public_path().$doc->filename)){
                unlink(public_path().$doc->filename);
            }

            $doc->delete();
            return redirect()->route('documents.index')->with('success', 'The document has been deleted!');
        } else {
            return back();
        }
    }
}
