@extends('layouts.admin')

@section('page_title')
    <h1>
        Sessions
    </h1>
@endsection

@section('breadcrumb')
    <li><a href="{{route('session.index')}}">Sessions</a></li>
    <li class="active"><a href="{{route('session.create')}}">Create</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8 col-sm-6 col-xs-8">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">New session</h3>
                </div>
                <div class="box-body">
                    <form action="{{route('session.store')}}" method="post">
                        @csrf
                        @method('POST')

                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" name="name" class="form-control" id="name" required>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <label for="from">From</label>
                                <input type="text" name="from" class="datepicker form-control" placeholder="Registration starting date" required>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <label for="to">To</label>
                                <input type="text" name="to" class="datepicker form-control" placeholder="Registration closing date" required>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <label for="birth_min">DOB min</label>
                                <input type="text" name="birth_min" class="datepicker form-control" placeholder="Minimum date of birth date" required>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <label for="birth_max">DOB max</label>
                                <input type="text" name="birth_max" class="datepicker form-control" placeholder="Maximum date of birth date" required>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <label for="year">Year</label>
                                <input type="number" min="2000" name="year" id="year" class="form-control" required>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <label for="max_count">Max.Count</label>
                                <input type="number" name="max_count" min="0" id="max_count" class="form-control" required>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <label for="is_current">Is current</label>
                                <select name="is_current" id="is_current" class="form-control" required>
                                    <option value="0">No</option>
                                    <option value="1">Yes</option>
                                </select>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <label for="status">Status</label>
                                <select name="status" id="status" class="form-control" required>
                                    <option value="0">Closed</option>
                                    <option value="1">Open</option>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="text-right">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-plus fa-fw"></i> Create</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12"></div>
        <div class="clearfix"></div>
    </div>
@endsection
