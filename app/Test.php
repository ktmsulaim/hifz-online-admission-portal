<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{

    protected $fillable = [
        'session_id',
        'name',
        'details',
        'pass_mark',
        'max_mark',
        'status'
    ];

    public function session()
    {
        return $this->belongsTo('App\Session');
    }

    public function applicants()
    {
        return $this->belongsToMany('App\Applicant')->withPivot('mark', 'remark', 'status');
    }

}
