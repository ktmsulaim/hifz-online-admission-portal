@extends('layouts.client')

@section('page_title')
    <h1>Applications</h1>
@endsection

@section('breadcrumb')
    <li><a href="{{route('application.index')}}">Applications</a></li>
    <li class="active"><a href="{{route('application.show', $application->id)}}">{{$application->refno()}}</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="box box-widget widget-user-2">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-aqua-active">
                  <div class="widget-user-image">
                    <img class="img-circle" width="60" height="60" src="{{$application->hasPhoto() ? asset($application->photo->filename) : asset('img/150.png')}}" alt="{{$application->name}}">
                  </div>
                  <!-- /.widget-user-image -->
                  <h3 class="widget-user-username">{{$application->name}}</h3>
                  <h5 class="widget-user-desc">&nbsp;</h5>
                </div>
                <div class="box-footer no-padding">
                  <ul class="nav nav-stacked">
                    <li><a><strong>Ref.no: </strong> <span>{{$application->refno()}}</span></a></li>
                    <li><a><strong>DOB: </strong> <span>{{$application->dob}}</span></a></li>
                    <li><a><strong>Age: </strong> <span>{{$application->dob_details}}</span></a></li>
                    <li><a><strong>Completed class: </strong> <span>{{$application->madrassa_completed_class}}</span></a></li>
                    <li><a class="clearfix"><strong>Status</strong> {!! $application->isVerified() ? '<span class="label label-success rounded">Verified</span>' : '<span class="label label-danger rounded">Not verified</span>' !!}</a></li>
                  </ul>
                </div>
              </div>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Personal info</h3>
                    <div class="box-tools">
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-bordered">
                        <tr>
                            <th>District</th>
                            <td>{{$application->district}}</td>
                        </tr>
                        <tr>
                            <th>Province</th>
                            <td>{{$application->province}}</td>
                        </tr>
                        <tr>
                            <th>Phone</th>
                            <td>{{$application->mobile}}</td>
                        </tr>
                        <tr>
                            <th>Landphone</th>
                            <td>{{$application->landphone_std.' '.$application->landphone}}</td>
                        </tr>
                        <tr>
                            <th>Name of madrassa</th>
                            <td>{{$application->name_of_madrassa}}</td>
                        </tr>
                        <tr>
                            <th width="150">Madrassa number</th>
                            <td>{{$application->madrassa_number}}</td>
                        </tr>
                    </table>
                </div>
                <div class="box-footer">
                   <div class="btn-group-sm">
                    @if ((int)$current->status === 1)
                        <a href="{{route('application.edit', $application->id)}}" data-toggle="tooltip" data-original-title="Edit" class="btn btn-link"><i class="fa fa-edit fa-fw"></i> Edit</a>
                    @endif
                    @if ($current->form() && file_exists(public_path().$current->form()->filename))
                        <button form="print" data-toggle="tooltip" data-original-title="Print" class="btn btn-link" type="submit"><i class="fa fa-print fa-fw"></i> Print form</button>
                        <form id="print" action="{{url('/client/application/'.$application->id.'/print')}}" target="_blank" method="get">

                        </form>
                    @endif
                   </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Verification</h3>
                </div>
                <div class="box-body">
                    @if (count($types) > 0)
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                          @foreach ($types as $key => $type)
                          <li class="{{$key === 0 ? 'active' : ''}}"><a href="#tab_{{$type->id}}" data-toggle="tab">{{$type->name}}</a></li>
                          @endforeach
                        </ul>
                        <div class="tab-content">
                          @foreach ($types as $key => $type)
                          <div class="tab-pane {{$key === 0 ? 'active' : ''}}" id="tab_{{$type->id}}">
                               @if (count($application->verificationItems) > 0)
                               <ol class="todo-list ui-sortable">
                                @foreach ($application->verificationItems as $vitem)
                                    @if ($vitem->type_id === $type->id)
                                    <li class="clearfix">
                                        <div class="pull-left">
                                            <span class="text"><label for="item{{$vitem->id}}">{{$vitem->name}}</label></span>
                                            @if ($vitem->status($application->id))
                                            <small class="label label-success"><i class="fa fa-check-circle-o"></i> Verified</small>
                                            @else
                                            <small class="label label-danger"><i class="fa fa-times-circle"></i> Not verified</small>
                                            @endif

                                            @if (!empty($vitem->pivot->remark) && (int)$vitem->pivot->remark_status === 1)
                                                <span class="remarks">{{$vitem->pivot->remark}}</span>
                                            @endif
                                        </div>
                                    </li>
                                    @endif
                                @endforeach
                               </u>
                               @endif
                          </div>
                          @endforeach
                          <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                      </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
           <div class="box {{$application->hasApplicant() ? 'box-primary' : 'box-warning'}}">
               <div class="box-header with-border">
                   <h3 class="box-title">Section details</h3>
                   <div class="box-tools">
                        @if ($application->hasApplicant() && $application->applicant->hasSection() && $current->status === 1)
                        <button type="button" class="btn btn-box-tool" data-toggle="modal" data-target="#changeSection">
                            Change
                          </button>

                          <div class="modal fade" id="changeSection">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">Change section</h4>
                                </div>
                                <div class="modal-body">
                                  <form id="change-section" action="{{route('changeSection')}}" method="post">
                                      @csrf
                                      @method('POST')
                                    <input type="hidden" name="app" value="{{$application->hasApplicant() ? $application->applicant->id : 0}}">

                                    <p>Your current section is: <strong>{{$application->applicant->getSection()->name.' - '.$application->applicant->getSection()->details}}</strong></p>

                                    <div class="form-group">
                                        <input type="hidden" name="app" value="{{$application->id}}">
                                        <select name="section" id="section" class="form-control" required>
                                            @if (count($sections) > 0)
                                                <option value="">Select a section</option>
                                                @foreach ($sections as $section)
                                                    <option value="{{$section->id}}" {{$section->checkAvail() === false || $application->applicant->getSection()->id === $section->id ? 'disabled' : '' }}>{{$section->name. ' ('. $section->details.') '}} {{$section->avail() > 1 ? $section->avail()." seats left" : $section->avail()." seat left"}}</option>
                                                @endforeach
                                            @else
                                                <option value="">No section available</option>
                                            @endif
                                        </select>
                                    </div>
                                  </form>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                  <button form="change-section" type="submit" class="btn btn-primary">Save changes</button>
                                </div>
                              </div>
                              <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                          </div>
                          <!-- /.modal -->
                        @endif
                   </div>
               </div>
               <div class="box-body">
                @if ($application->hasApplicant())
                    @if ($apt->hasSection())
                        <table class="table table-bordered">
                            <tr>
                                <th width="100">Section</th>
                                <td>{{$apt->getSection()->name}}</td>
                            </tr>
                            <tr>
                                <th width="100">Timing</th>
                                <td>{{$apt->getSection()->details}}</td>
                            </tr>
                            <tr>
                                <th width="100">Date</th>
                                <td>{{$apt->getSection()->start . ' - '. $apt->getSection()->end}}</td>
                            </tr>
                            <tr>
                                <th width="100">Serial no</th>
                                <td>{{$apt->section->first()->pivot->token}}</td>
                            </tr>
                            <tr>
                                <th width="100">Appearance</th>
                                <td>{!! $apt->getSection()->pivot->appear === 0 ? '<span class="label label-danger">Not appreared</span>' : '<span class="label label-success">Appreared</span><i style="margin-left: 8px" class="fa fa-clock-o fa-fw"></i> '. date('F d, g:i A', strtotime($apt->getSection()->pivot->appeartime)) !!}</td>
                            </tr>
                        </table>
                    @elseif($current->status === 0)
                    <p class="text-muted"><i class="fa fa-frown-o fa-fw"></i> Sorry! you cannot select a section right now!</p>
                    @else
                    <p class="text-muted">You have to select a section to appear in the interview.</p>
                    <form action="{{route('setSection')}}" method="post">
                        @csrf
                        @method('POST')
                        <div class="form-group">
                            <input type="hidden" name="app" value="{{$application->id}}">
                            <select name="section" id="section" class="form-control" required>
                                @if (count($sections) > 0)
                                    <option value="">Select a section</option>
                                    @foreach ($sections as $section)
                                        <option value="{{$section->id}}" {{$section->checkAvail() ? '' : 'disabled'}}>{{$section->name. ' ('. $section->details.') '}} {{$section->avail() > 1 ? $section->avail()." seats left" : $section->avail()." seat left"}}</option>
                                    @endforeach
                                @else
                                    <option value="">No section available</option>
                                @endif
                            </select>
                        </div>
                        <div class="text-right">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    </form>
                    @endif
                @else
                <p class="text-muted"><i class="fa fa-frown-o fa-fw"></i> The applicant is not qualified to appear in the interview!</p>
                @endif
               </div>
                   @if ($application->hasApplicant() && $apt->hasSection())
                   <div class="box-footer">
                        @if (file_exists(public_path().$current->hallTicket()->filename))
                            <form action="{{route('getHallTicket', $application->id)}}" method="get" target="_blank">
                                    <button class="btn-simple" type="submit"><i class="fa fa-download fa-fw"></i> Download hall ticket</button>
                            </form>
                        @endif
                    </div>
                   @endif
           </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            @if ($application->hasApplicant() && $apt->hasSection())
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Results</h3>
                </div>
                <div class="box-body">
                    @if ($current->hasResult() && $current->results->first()->status === 1)
                        <table class="table table-bordered">
                            <tr>
                                <th>Marks</th>
                                <td>{{$apt->marks()}}%</td>
                            </tr>
                            @if ($apt->passed())
                                <tr>
                                    <th>Rank</th>
                                    <td>{{$apt->rank}}</td>
                                </tr>
                            @endif
                            <tr>
                                <th>Status</th>
                                <td>
                                    @if ($apt->status === 1)
                                        <span class="label label-success">Enrolled</span>
                                    @elseif($apt->status === 2)
                                        <span class="label label-info">Waiting list</span>
                                    @else
                                        <span class="label label-danger">Not enrolled</span>
                                    @endif
                                </td>
                            </tr>
                        </table>
                    @else
                        <p class="text-muted text-center"><i class="fa fa-frown-o fa-fw"></i> Sorry! The result wasn't published. Stay tuned!</p>
                    @endif
                </div>
            </div>
            @endif
        </div>
        <div class="clearfix"></div>
    </div>
@endsection
