<?php

namespace App\Http\Controllers;

use App\Type;
use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class TypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Gate::allows('verification_access')) {
            $types = Type::all();
            return view('admin.validation.types.index', compact('types'));
        } else {
            return back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Gate::allows('verification_access')) {
            $types = Type::all();
            return view('admin.validation.types.index', compact('types'));
        } else {
            return back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Gate::allows('verification_access')) {
            if(!empty($request->name)){
                $type = Type::create($request->all());
                return redirect()->route('types.index');
            }else {
                return redirect()->route('types.create');
            }
        } else {
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function show(Type $type)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Gate::allows('verification_access')) {
            $types = Type::all();
            $t = Type::findOrFail($id);
            return view('admin.validation.types.edit', compact('types', 't'));
        }else{
            return back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Gate::allows('verification_access')){
            $type = Type::findOrFail($id);
            if(!empty($request->name)){
                $type->update($request->all());
                return redirect()->route('types.index');
            }
        }else{
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Gate::allows('verification_access')){
            $type = Type::findOrFail($id);
            $type->delete();
            return redirect()->route('types.index');
        }else{
            return back();
        }
    }
}
