@extends('layouts.client')

@section('page_title')
    <h1>About</h1>
@endsection

@section('breadcrumb')
    <li><a href="{{route('about')}}">About</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <div class="box">
                <div class="box-body">
                    <div class="box-image text-center">
                        <img class="img-responsive" src="{{asset('img/hasanath.jpg')}}" alt="">
                    </div>
                    <div class="about-title">
                        <h2>{{$about ? $about->name : ''}}</h2>
                    </div>
                    <div class="about-desc">
                        <p>{{$about ? $about->description : ''}}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Contact us</h3>
                </div>
                <div class="box-body">
                    <div class="adress">
                        <strong><i class="fa fa-map-marker fa-fw margin-r-5"></i> Address</strong>
                        <p class="text-muted">{{$about ? $about->address : ''}}</p>
                    </div>
                    <hr>
                    <div class="contact">
                        <strong><i class="fa fa-phone fa-fw margin-r-5"></i> Phone</strong>
                        <p class="text-muted">{{$about ? $about->contact : ''}}</p>
                    </div>
                    <hr>
                    <div class="contact2">
                        <strong><i class="fa fa-phone fa-fw margin-r-5"></i> Phone 2</strong>
                        <p class="text-muted">{{$about ? $about->contact2 : ''}}</p>
                    </div>
                    <hr>
                    <div class="email">
                        <strong><i class="fa fa-envelope fa-fw margin-r-5"></i> Email</strong>
                        <p class="text-muted">{{$about ? $about->email : ''}}</p>
                    </div>
                    <hr>
                    <div class="website">
                        <strong><i class="fa fa-globe fa-fw margin-r-5"></i> Website</strong>
                        <p class="text-muted">{{$about ? $about->website : ''}}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
@endsection