<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VerificationItem extends Model
{
    protected $fillable = ['type_id', 'name'];

    public function type()
    {
        return $this->belongsTo('App\Type');
    }

    public function applications()
    {
        return $this->belongsToMany('App\Application')->withPivot(['status', 'remark','remark_status', 'approvedby', 'created_at', 'updated_at']);
    }

    public function status($app)
    {
        if($this->applications()->where('application_verification_item.application_id', '=', $app)->first()->pivot->status === 1) {
            return true;
        }

        return false;
    }
}
