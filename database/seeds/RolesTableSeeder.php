<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'name' => 'admin'
        ]);

        Role::create([
            'name' => 'helpdesk'
        ]);

        Role::create([
            'name' => 'examiner'
        ]);

        Role::create([
            'name' => 'standard'
        ]);
    }
}
