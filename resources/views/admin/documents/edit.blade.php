@extends('layouts.admin')

@section('page_title')
    <h1>
        Documents
    </h1>
@endsection

@section('breadcrumb')
    <li><a href="{{route('documents.index')}}">Documents</a></li>
    <li class="active"><a href="{{route('documents.edit', $doc->id)}}">Edit</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8 col-lg-8 col-sm-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit document</h3>
                </div>
                <div class="box-body">
                    <form action="{{route('documents.update', $doc->id)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')

                        <input type="hidden" name="session_id" value="{{$session->id}}">

                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" name="title" id="title" class="form-control" value="{{old('title', $doc->title)}}" required>
                        </div>

                        <div class="form-group">
                            <label for="details">Details (Optional)</label>
                            <textarea style="resize:none" name="details" id="details" class="form-control" cols="30" rows="5">{{old('details', $doc->details)}}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="type">Type</label>
                            <select name="type" id="type" class="form-control" required>
                                <option value="">Select a document type</option>
                                <option value="1" {{$doc->type === 1 ? 'selected' : ''}} {{\App\Document::hasForm() === true && $doc->type !== 1 ? 'disabled' : ''}}>Application form</option>
                                <option value="2" {{$doc->type === 2 ? 'selected' : ''}}{{\App\Document::hasTicket() === true && $doc->type !== 2 ? 'disabled' : ''}}>Hall ticket</option>
                                <option value="0" {{$doc->type === 0 ? 'selected' : ''}}>Other</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="filename">File (.pdf)</label>
                            <input type="file" accept=".pdf" name="filename" id="filename" class="form-control">
                        </div>

                        <div class="text-right">
                            <button class="btn btn-primary" type="submit"><i class="fa fa-plus fa-fw"></i> Update</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
            @if (count($errors) > 0)
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Error</h3>
                    </div>
                    <div class="box-body">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif

            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">Delete</h3>
                </div>
                <div class="box-body">
                    <p>You are going to delete this document. This can't recover any lost data once it is deleted. continue?</p>
                    <form action="{{route('documents.destroy', $doc->id)}}" method="post">
                        @csrf
                        @method('DELETE')
                        <div class="text-right">
                            <button class="btn btn-danger" type="submit"><i class="fa fa-trash-o fa-fw"></i> Delete</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(function(){
            $('#filename').fileinput({
                maxFileSize: 1024,
                showUpload: false,
                allowedFileExtensions: ['pdf'],
                initialPreviewAsData: true,
                initialPreviewFileType: 'pdf', 
                initialPreview: '{{asset($doc->filename)}}'
            });
        });
    </script>
@endsection
