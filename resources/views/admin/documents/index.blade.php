@extends('layouts.admin')

@section('page_title')
    <h1>
        Documents
    </h1>
@endsection

@section('breadcrumb')
    <li class="active"><a href="{{route('documents.index')}}">Documents</a></li>
@endsection

@section('content')
    @if ($message = Session::get('success'))
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Success!</h4>
                {{$message}}
            </div>
        </div>
    </div>
    @endif
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">All documents <small>{{$session->name}}</small></h3>

                    <div class="box-tools">
                        <a href="{{route('documents.create')}}" class="btn btn-primary btn-sm"><i class="fa fa-plus fa-fw"></i> New</a>
                    </div>
                </div>
                <div class="box-body no-padding">
                    @if (count($docs) > 0)
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Preview</th>
                                    <th>Title</th>
                                    <th>Type</th>
                                    <th>Details</th>
                                    <th width="50"></th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                    @foreach ($docs as $key => $doc)
                                        <tr>
                                            <td>{{$key + 1}}</td>
                                            <td><iframe src="{{asset('/ViewerJS/#..'.$doc->filename)}}" width="" height="200" frameborder="0" allowfullscreen webkitallowfullscreen></iframe></td>
                                            <td>{{$doc->title}}</td>
                                            <td>
                                                @if ($doc->type === 1)
                                                    {{'Application form'}}
                                                @elseif($doc->type === 2)
                                                    {{'Hall ticket'}}
                                                @else
                                                    {{'Other'}}
                                                @endif
                                            </td>
                                            <td>{{$doc->details}}</td>
                                            <td><a href="{{route('documents.edit', $doc->id)}}"><i class="fa fa-edit fa-fw"></i></a></td>
                                        </tr>
                                    @endforeach
                            
                            </tbody>
                        </table>
                    @else
                        <p class="text-center text-muted box-body"><i class="fa fa-frown-o fa-fw"></i> No document found!</p>
                    @endif
                </div>
                @if (count($docs) > 0)
                    <div class="box-footer">
                        {{$docs->links()}}
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection