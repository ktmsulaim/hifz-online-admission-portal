<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $fillable = ['filename'];
    protected $dir = '/images/';

    public function photoable()
    {
        return $this->morphTo();
    }

    public function getFilenameAttribute($val)
    {
        if(!empty($val)){
            return $this->dir.$val;
        }
    }
}
