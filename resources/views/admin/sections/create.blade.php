@extends('layouts.admin')

@section('page_title')
    <h1>Examination</h1>
@endsection

@section('breadcrumb')
    <li><a href="{{route('sections.index')}}">Sections</a></li>
    <li class="active"><a href="{{route('sections.create')}}">Create</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Create section
                    </h3>
                </div>
                <div class="box-body">
                    <form action="{{route('sections.store')}}" method="post">
                        @csrf
                        @method('POST')
                        <input type="hidden" name="session_id" value="{{$current->id}}">

                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" name="name" id="name" class="form-control" required>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="start">Start</label>
                                <input type="text" name="start" id="start" class="form-control datetime" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="end">End</label>
                                <input type="text" name="end" id="end" class="form-control datetime" required>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="details">Details</label>
                                <input type="text" name="details" id="details" class="form-control" placeholder="8 AM - 9 AM" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="count">Count</label>
                                <input type="number" min="0" name="count" id="count" placeholder="Maximum count of applicants" class="form-control" required>
                            </div>
                        </div>

                        <div class="text-right">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-plus fa-fw"></i> Create</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(function(){
            $('.datetime').datetimepicker();
        });
    </script>
@endsection
